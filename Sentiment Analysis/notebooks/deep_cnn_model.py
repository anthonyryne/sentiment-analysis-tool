import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))

import numpy as np
import pandas as pd

print("reading data")
big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)

print("sampling data")
SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

print("splitting data")
from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)

x_all = pd.concat([x_train, x_val, x_test], ignore_index=True)
y_all = pd.concat([y_train, y_val, y_test], ignore_index=True)


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

print("getting maximum lengh")
def get_max_len(arr):
    return max(arr.apply(lambda a :len(a.split())))

max_len = get_max_len(x_all) + 15

print("tokenizing and fitting")
tok = Tokenizer(num_words=100000)
tok.fit_on_texts(x_train)

def get_sequences(tok, x_train, x_val, x_test):
    train_sequences = tok.texts_to_sequences(x_train)
    val_sequences = tok.texts_to_sequences(x_val)
    test_sequences = tok.texts_to_sequences(x_test)
    x_train_seq = pad_sequences(train_sequences, maxlen=max_len)
    x_val_seq = pad_sequences(val_sequences, maxlen=max_len)
    x_test_seq = pad_sequences(test_sequences, maxlen=max_len)
    return x_train_seq, x_val_seq, x_test_seq

print("getting sequences")
x_train_seq, x_val_seq, x_test_seq = get_sequences(tok, x_train, x_val, x_test)

from gensim.models import KeyedVectors

def get_concat_vocab(): 
    w2v_cbow_vec = KeyedVectors.load("w2v_cbow.kv")
    w2v_sg_vec = KeyedVectors.load("w2v_sg.kv")
    concatenated_vocab = dict()
    for word in w2v_cbow_vec.wv.vocab.keys():
        word_as_arr = np.append(w2v_cbow_vec.wv[word], w2v_sg_vec.wv[word]) 
        concatenated_vocab[word] = word_as_arr
    return concatenated_vocab

print("getting concatenated vocabulary")
concat_vocab = get_concat_vocab()

def get_weights(tok, concatenated_vocab, x_train, x_val):
    weights = np.zeros((100000, 200))
    for word, ind in tok.word_index.items():
        if ind > 100000:
            continue
        weights[ind - 1] = concatenated_vocab[word]
    return weights

print("getting weights")
weights = get_weights(tok, concat_vocab, x_train, x_val)

from keras.models import Model
from keras.layers import Embedding, Conv1D, GlobalMaxPool1D
from keras.layers import Input, concatenate, Dropout, Dense
from keras.callbacks import ModelCheckpoint

print("model creation")
data_input = Input((max_len,))
encoder = Embedding(100000, 200, weights=[weights], input_length=max_len, trainable=True)(data_input)
bigram = Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu")(encoder)
bigram = GlobalMaxPool1D()(bigram)
trigram = Conv1D(filters=100, kernel_size=3, strides=1, padding="valid", activation="relu")(encoder)
trigram = GlobalMaxPool1D()(trigram)
fourgram = Conv1D(filters=100, kernel_size=4, strides=1, padding="valid", activation="relu")(encoder)
fourgram = GlobalMaxPool1D()(fourgram)
grams = concatenate([bigram, trigram, fourgram], axis=1)

dense = Dense(256, activation="relu")(grams)
dropout = Dropout(0.2)(dense)
output = Dense(1, activation="sigmoid")(dropout)

model = Model(inputs=[data_input], outputs=[output])
model.compile(optimizer="adam", 
              loss="binary_crossentropy", 
              metrics=["accuracy"])

file_path = "cnn_weights.{epoch:02d}-{val_acc:.4f}.hdf5"
checkpoint = ModelCheckpoint(file_path, monitor="val_acc", save_best_only=True, verbose=1)

print("model fitting")
model.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), batch_size=32, epochs=5, verbose=2, callbacks=[checkpoint])

# from keras.models import load_model

# print("loading and evaluating second model")
# loaded_model = load_model("cnn_weights.03-0.9502.hdf5")
# print(loaded_model.evaluate(x=x_train_seq, y=y_train))
# print(loaded_model.evaluate(x=x_val_seq, y=y_val))
# print(loaded_model.evaluate(x=x_test_seq, y=y_test))


