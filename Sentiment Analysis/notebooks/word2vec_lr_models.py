import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)

SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

from sklearn.model_selection import train_test_split

x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)

x_all = pd.concat([x_train, x_val, x_test], ignore_index=True)
y_all = pd.concat([y_train, y_val, y_test], ignore_index=True)

def get_model_info(model, x_test, y_test):
    y_predicted = model.predict(x_test)
    accuracy = accuracy_score(y_test, y_predicted)
    print("accuracy: {}".format(accuracy))
    conf = confusion_matrix(y_predicted, y_test)
    conf_df = pd.DataFrame(conf, index=["real 1", "real 0"], columns=["predicted 1", "predicted 0"])
    print("Confusion Matrix:")
    print(conf_df)
    tp, fp, fn, tn = conf.ravel()
    precision = tp/(tp+fp)
    recall = tp/(tp+fn)
    f1 = 2 * precision * recall / (precision + recall)
    print("precision: {}".format(precision))
    print("recall: {}".format(recall))
    print("f1: {}".format(f1))

def sentence_to_arr(sentence, vectors, feature_num):
    words = sentence.split()
    result = np.zeros((len(words), feature_num))
    for ind, word in enumerate(words):
        if word in vectors:
            word_arr = vectors[word].reshape(1, feature_num)
            result[ind] = word_arr
    result = result[(result != 0).any(axis=1)]
    if result.size == 0:
        return np.array([np.nan] * feature_num)
    result = np.mean(result, axis=0)
    return result

def remove_indexes_from_arr(arr, indexes):
    mask = np.ones(arr.shape, dtype=bool)
    mask[indexes] = False
    return arr[mask]

def get_x_y_from_vectors(vectors, feature_num, x, y):
    x_new = np.array([sentence_to_arr(elem, vectors, feature_num) for elem in x])
    nan_indexes = np.unique(np.argwhere(np.isnan(x_new))[:,0])
    x_new = remove_indexes_from_arr(x_new, nan_indexes).reshape(-1, feature_num)
    y_new = remove_indexes_from_arr(y, nan_indexes)
    return x_new, y_new

from gensim.models import Word2Vec
import multiprocessing

def get_word2vec_model(skip_gram=0):
    cpu_num = multiprocessing.cpu_count()
    feature_num = 100
    max_distance = 5
    noise_words = 5
    minimum_word_num = 5
    w2v_cbow = Word2Vec(sg=skip_gram, size=feature_num, workers=cpu_num, negative=noise_words, window=max_distance, 
                    min_count=minimum_word_num)
    return w2v_cbow

from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

def train(model, sentences):
    learning_rate = 0.05
    min_learning_rate = 0.01
    epoch_num = 10
    learning_rate_delta = (learning_rate - min_learning_rate) / (epoch_num - 1)
    current_learning_rate = learning_rate
    for epoch in range(epoch_num):
        current_sentences = shuffle(sentences)
        model.alpha, model.min_alpha = current_learning_rate, current_learning_rate
        model.train(current_sentences, total_examples=len(current_sentences), epochs=1)
        print("epoch {} completed with alpha {}".format(epoch, current_learning_rate))
        current_learning_rate -= learning_rate_delta

w2v_data = x_all.apply(str.split)

def evaluate_and_get_model(x_train, y_train, x_val, y_val):
    lr = LogisticRegression()
    lr.fit(x_train,  y_train)
    get_model_info(lr, x_val, y_val)
    return lr

def execute_model():
    w2v_cbow = get_word2vec_model(skip_gram=0)
    w2v_sg = get_word2vec_model(skip_gram=1)
    cbow_vectors = list()
    sg_vectors = list()
    for ind, model in enumerate([w2v_cbow, w2v_sg]):
        model.build_vocab(w2v_data)
        train(model, w2v_data)
        w2v_x_train, w2v_y_train = get_x_y_from_vectors(model.wv, 100, x_train, y_train)
        w2v_x_val, w2v_y_val = get_x_y_from_vectors(model.wv, 100, x_val, y_val)
        evaluate_and_get_model(w2v_x_train, w2v_y_train, w2v_x_val, w2v_y_val)
        if ind == 0:
            cbow_vectors.append(w2v_x_train)
            cbow_vectors.append(w2v_x_val)
        else:
            sg_vectors.append(w2v_x_train)
            sg_vectors.append(w2v_x_val)
    w2v_x_train = np.concatenate([cbow_vectors[0], sg_vectors[0]], axis=1)
    w2v_x_val = np.concatenate([cbow_vectors[1], sg_vectors[1]], axis=1)
    evaluate_and_get_model(w2v_x_train, w2v_y_train, w2v_x_val, w2v_y_val)
    
execute_model()

