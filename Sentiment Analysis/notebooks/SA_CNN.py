
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')


# In[2]:


import sys
print(sys.executable)


# In[3]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# # Data reading

# In[4]:


big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)
data.head()


# # Data cleaning

# In[4]:


def include_word(word):
    forbidden_words = ["http", "www"]
    for forbidden_word in forbidden_words:
            return False
    return True


# In[5]:


def parse_word(word):
    word = re.sub("[^a-z A-Z]", "", word.lower().strip())
    return word


# In[6]:


def parse_sentence(sentence):
    parse_no_dict = {"won't": "will not", "can't": "can not", "n't": " not"}
    for k,v in parse_no_dict.items():
        sentence = sentence.replace(k, v)
    return " ".join([parse_word(word) for word in sentence.split(" ") if include_word(word)])


# In[7]:


def parse_df(df):
    new_df = df.copy()
    new_df["text"] = df["text"].apply(lambda sentence: parse_sentence(sentence))
    new_df["sentiment"] = new_df["sentiment"].apply(lambda x: 0 if x == 1 else 1)
    return new_df


# In[8]:


ALREADY_PARSED = True
if not ALREADY_PARSED:
    data = parse_df(data)
    print(data.head())
    data.to_csv(data_file, index=False)


# In[9]:


print('\033[1m' + "Df Info" + '\033[0m')
print(data.info())
print('\033[1m' + "Do We Have Any Null Values" + '\033[0m')
print(data.isnull().values.any() or data.isnull().values.any())


# # Data Visualization

# In[10]:


avereage_word_num = data["text"].apply(lambda x: len(x.split(" "))).mean()
print("there is {} word in sentence, on average".format(int(avereage_word_num)))


# In[11]:


positive_texts= data[data["sentiment"] == 1].text.values
negative_texts = data[data["sentiment"] == 0].text.values
print(len(positive_texts))
print(len(negative_texts))


# In[4]:


from sklearn.feature_extraction.text import CountVectorizer


# In[12]:


cvec = CountVectorizer(stop_words="english", max_features=100000)
cvec.fit(data["text"])
print(len(cvec.get_feature_names()))
cvec


# In[13]:


neg_transform = cvec.transform(negative_texts)
pos_transform = cvec.transform(positive_texts)
neg_transform.shape


# In[14]:


neg_matrix = np.sum(neg_transform, axis=0)
pos_matrix = np.sum(pos_transform, axis=0)
neg_matrix.shape


# In[15]:


neg = np.squeeze(np.asarray(neg_matrix))
pos = np.squeeze(np.asarray(pos_matrix))
neg.shape


# In[16]:


term_freq_df = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).T
term_freq_df.columns = ["negative", "positive"]
term_freq_df["total"] = term_freq_df["negative"] + term_freq_df["positive"]
term_freq_df.sort_values(by=["total", "negative", "positive"], inplace=True, ascending=False)
term_freq_df.head()


# In[17]:


top_num = 50
neg_words = term_freq_df.sort_values(by='negative', ascending=False)['negative']
top_neg_words = neg_words[:top_num]
x = np.arange(top_num)
plt.figure(figsize=(12,10))
plt.bar(x, top_neg_words)
plt.xticks(x, top_neg_words.index,rotation='vertical')
plt.xlabel('Top {} negative tokens'.format(top_num))
plt.ylabel('Frequency')
plt.title('Top {} tokens in negative amazon data'.format(top_num))


# In[18]:


top_num = 50
pos_words = term_freq_df.sort_values(by='positive', ascending=False)['positive']
pos_neg_words = pos_words[:top_num]
x = np.arange(top_num)
plt.figure(figsize=(12,10))
plt.bar(x, pos_neg_words)
plt.xticks(x, pos_neg_words.index,rotation='vertical')
plt.xlabel('Top {} positive tokens'.format(top_num))
plt.ylabel('Frequency')
plt.title('Top {} tokens in positive amazon data'.format(top_num))


# In[19]:


term_freq_df["positive_rate"] = term_freq_df["positive"] / term_freq_df["total"]
term_freq_df.head()


# # Data splitting

# In[5]:


SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)
data.head()


# In[6]:


from sklearn.model_selection import train_test_split


# In[7]:


from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)


# In[7]:


train_positive_percent = round(len(x_train[y_train == 1])/len(x_train),3)
val_positive_percent = round(len(x_val[y_val == 1])/len(x_val), 3)
test_positive_percent = round(len(x_test[y_test == 1])/len(x_test), 3)
print("train set contains {} examples where {} is positive".format(x_train.shape[0], train_positive_percent))
print("validation set contains {} examples where {} is positive".format(x_val.shape[0], val_positive_percent))
print("test set contains {} examples where {} is positive".format(x_test.shape[0], test_positive_percent))


# In[8]:


x_all = pd.concat([x_train, x_val, x_test], ignore_index=True)
y_all = pd.concat([y_train, y_val, y_test], ignore_index=True)


# # Modeling

# In[12]:


from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle


# In[5]:


# pipeline = Pipeline([("cv", CountVectorizer(max_features=100000)), ("lr", LogisticRegression())])
# fitted = pipeline.fit(x_train, y_train)
# y_predicted = fitted.predict(x_val)
# accuracy_score(y_val, y_predicted)


# In[15]:


file = "lr_v1.pkl"
with open(file, "rb") as f:
    model = pickle.load(f)


# In[16]:


y_predicted = model.predict(x_test)


# In[9]:


def get_model_info(model, x_test, y_test):
    y_predicted = model.predict(x_test)
    accuracy = accuracy_score(y_test, y_predicted)
    print("accuracy: {}".format(accuracy))
    conf = confusion_matrix(y_predicted, y_test)
    conf_df = pd.DataFrame(conf, index=["real 1", "real 0"], columns=["predicted 1", "predicted 0"])
    print("Confusion Matrix:")
    print(conf_df)
    tp, fp, fn, tn = conf.ravel()
    precision = tp/(tp+fp)
    recall = tp/(tp+fn)
    f1 = 2 * precision * recall / (precision + recall)
    print("precision: {}".format(precision))
    print("recall: {}".format(recall))
    print("f1: {}".format(f1))


# In[28]:


get_model_info(model, x_test, y_test)


# In[10]:


def get_error_indexes(y_predicted, y_test, predicted_val, real_val):
    wrong_prediction_df = pd.DataFrame([y_predicted, y_test]).T
    wrong_prediction_df.columns = ["predicted", "real"]
    wrong_prediction_df["predicted"] = wrong_prediction_df[wrong_prediction_df["predicted"] == predicted_val]
    wrong_prediction_df["real"] = wrong_prediction_df[wrong_prediction_df["real"] == real_val]
    wrong_prediction_df = wrong_prediction_df.dropna()
    return wrong_prediction_df.index


# In[11]:


def get_fp_indexes(y_predicted, y_test):
    return get_error_indexes(y_predicted, y_test, 1, 0)


# In[12]:


def get_fn_indexes(y_predicted, y_test):
    return get_error_indexes(y_predicted, y_test, 0, 1)


# In[32]:


fp_indexes = get_fp_indexes(y_predicted, y_test)
fn_indexes = get_fn_indexes(y_predicted, y_test)
print(len(fp_indexes))
print(len(fn_indexes))


# In[33]:


fp_sentences = x_test.iloc[fp_indexes]
fn_sentences = x_test.iloc[fn_indexes]


# In[34]:


cur_sentences = fp_sentences
for ind, cur_sentence in enumerate(cur_sentences):
    print(cur_sentences.index[ind])
    print(cur_sentence)
    br = input()
    if br == "1":
        break


# In[35]:


# sentence = "its bad. Joking, its really super good"
# sentence_series = pd.Series(sentence)
# result = new_fitted.predict(sentence_series)
# result[0]


# In[12]:


cv1 = CountVectorizer(max_features=100000)
cv2 = CountVectorizer(max_features=120000)
cv3 = CountVectorizer(max_features=100000, stop_words="english")
cv4 = CountVectorizer(max_features=100000, ngram_range=(1, 2))
cv5 = CountVectorizer(max_features=100000, ngram_range=(1, 3))
tvec1 = TfidfVectorizer(max_features=100000)
tvec2 = TfidfVectorizer(max_features=100000, ngram_range=(1,2))
tvec3 = TfidfVectorizer(max_features=100000, ngram_range=(1,3))
vectorizers = [cv1, cv2, cv3, cv4, cv5, tvec1, tvec2, tvec3]


# In[13]:


file_names = ["lr{}.pkl".format(ind + 1) for ind in range(len(vectorizers))]
# for ind, cv in enumerate(vectorizers):
#     pipeline = Pipeline([("cv", cv), ("lr", LogisticRegression())])
#     pipe = pipeline.fit(x_train, y_train)
#     with open(file_names[ind], "wb") as f:
#         pickle.dump(pipe, f)
#     print("done with index {}".format(ind))


# In[14]:


ind = 0
for file_name in file_names:
    if ind == 7:
            break
    with open(file_name, "rb") as f:
        print(file_name, ind)
        pipe = pickle.load(f, encoding="latin1")
        y_predicted = pipe.predict(x_test)
        get_model_info(pipe, x_test, y_test)
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        ind += 1


# In[10]:


from gensim.models import Doc2Vec, Word2Vec
from gensim.models.doc2vec import TaggedDocument


# In[11]:


small_data = data[:100000]


# In[60]:


sentences = [elem.split() for elem in small_data["text"].values]
sentences


# In[61]:


w2v = Word2Vec(sentences)


# In[62]:


w2v.train(sentences, total_examples=10000, epochs=1)


# In[17]:


w2v.wv.vectors.shape


# In[64]:


w2v.wv.vocab


# In[19]:


w2v.wv.most_similar(positive=["book"], negative=["movie"])
w2v.wv.similarity(w1="book", w2="pen")


# # DOC 2 VEC models starting

# In[12]:


import multiprocessing
cpu_num = multiprocessing.cpu_count()
cpu_num


# In[18]:


small_data = data[:100000]


# In[13]:


small_x = small_data["text"]
small_y = small_data["sentiment"]
small_x_train, small_x_test, small_y_train, small_y_test = train_test_split(small_x, small_y, test_size=0.2, random_state=SEED)
small_x_val, small_x_test, small_y_val, small_y_test = train_test_split(small_x_test, small_y_test, test_size=0.5, random_state=SEED)


# In[20]:


def get_tagged_data(data):
    return [TaggedDocument(elem.split(), ["ind{}".format(ind)]) for ind, elem in zip(data.index, data)]


# In[14]:


small_x_all = pd.concat([small_x_train, small_x_val, small_x_test], ignore_index=True)
small_y_all = pd.concat([small_y_train, small_y_val, small_y_test], ignore_index=True)


# In[22]:


from gensim.models.doc2vec import TaggedDocument


# In[20]:


tgd = get_tagged_data(small_x_all)
tgd


# In[17]:


features_num = 100
workers_num = cpu_num
minimum_word_num = 5
max_distance = 5
# noise_words = 5
noise_words = 5
learning_rate = 0.05
min_learning_rate = 0.01
epochs_num = 5
learning_rate_delta = (learning_rate - min_learning_rate) / (epochs_num - 1)


# In[15]:


from sklearn.utils import shuffle

def train(model):
    current_learning_rate = learning_rate
    for epoch in range(epochs_num):
        current_tgd = shuffle(tgd)
        model.alpha, model.min_alpha = current_learning_rate, current_learning_rate
        model.train(current_tgd, total_examples=len(current_tgd), epochs=1)
        print("epoch {} completed with alpha {}".format(epoch, current_learning_rate))
        current_learning_rate -= learning_rate_delta


# In[16]:


def evaluate_and_get_model(x, y):
    lr = LogisticRegression()
    lr.fit(x, y)
    get_model_info(lr, x, y)
    return lr


# In[32]:


# d2v.docvecs.vectors_docs.shape d2v[index]
# d2v.docvecs.index2entity


# In[34]:


d2v1 = Doc2Vec(dm=0, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
d2v2 = Doc2Vec(dm=1, dm_concat=1, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
d2v3 = Doc2Vec(dm=1, dm_mean=1, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
doc2vecs = [d2v1, d2v2, d2v3]
doc2vecs = [d2v1]


# In[35]:


x_s = list()
for ind, d2v in enumerate(doc2vecs):
    d2v.build_vocab(tgd)
    train(d2v)
    # d2v.save("d2v{}.d2v".format(ind + 1))
    x = d2v[d2v.docvecs.index2entity]
    x_s.append(x)
    y = small_y_all
    model = evaluate_and_get_model(x, y)
#     with open("lr_d2v{}.pkl".format(ind + 1), "wb") as f:
#         pickle.dump(model, f)


# In[39]:


for i in range(2):
    if len(x_s) == 1:
        break
    x1 = x_s[0]
    x2 = x_s[i+1]
    new_shape = (x1.shape[0], x2.shape[1]*2)
    x = np.stack((x1, x2), axis=1).reshape(new_shape)
    x_s.append(x)
    model = evaluate_and_get_model(x, y)
#     with open("lr_d2v{}.pkl".format(3 + i + 1), "wb") as f:
#         pickle.dump(model, f)


# In[17]:


from gensim.models.phrases import Phrases, Phraser


# In[27]:


ssxt = small_x_train.apply(str.split).tolist()
phrases = Phrases(ssxt)
bigram = Phraser(phrases)

TRIGRAM = True

if TRIGRAM:
    phrases = Phrases(bigram[small_x_train])
    trigram = Phraser(phrases)


# In[25]:


def get_phrased_tagged_data(data, ngram):
    return [TaggedDocument(ngram[elem.split(" ")], ["ind{}".format(ind)]) for ind, elem in zip(data.index, data)]


# In[29]:


tgd = get_phrased_tagged_data(small_x_all, bigram)
tgd


# In[26]:


d2v = Doc2Vec(dm=0, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
d2v.build_vocab(tgd)
train(d2v)


# In[28]:


x = d2v[d2v.docvecs.index2entity]
y = small_y_all
model = evaluate_and_get_model(x, y)


# In[21]:


from sklearn.feature_selection import chi2


# In[69]:


tfidf = TfidfVectorizer(max_features=10000, ngram_range=(1,2))
x_train_tfidf = tfidf.fit_transform(small_x_train, small_y_train)
x_val_tfidf = tfidf.transform(small_x_val)
feature_names = tfidf.get_feature_names()


# In[70]:


x_train_tfidf.shape


# In[26]:


def get_top_feature_names(vectorizer, x, y, top_n):
    chi2scores = chi2(x, y)[0]
    chi2scores = sorted(zip(feature_names, chi2scores), key=lambda x: x[1])[:top_n]
    return [feature_name for feature_name, score in chi2scores]


# In[72]:


top_feature_names = get_top_feature_names(tfidf, x_train_tfidf, small_y_train, 20)
top_feature_names


# In[27]:


from sklearn.feature_selection import SelectKBest


# In[94]:


for i in range(1000, 10000, 1000):
#     ch2 = SelectKBest(chi2, 20)
#     chi2_train = ch2.fit_transform(x_train_tfidf, small_y_train)
#     chi2_val = ch2.transform(x_val_tfidf)
    pipeline = Pipeline([("tfidf", TfidfVectorizer(max_features=10000, ngram_range=(1,2))), ("kbest", SelectKBest(chi2, i)), ("lr", LogisticRegression())])
    pipeline.fit(small_x_train, small_y_train)
    get_model_info(pipeline, small_x_val, small_y_val)


# # Neural Networks starting

# In[28]:


SEED = 9
np.random.seed(SEED)


# In[29]:


def batch_generator(X, y, batch_size=32):
    sample_num = X.shape[0]
    batch_num = sample_num / batch_size
    for ind in np.arange(sample_num):
        start_ind = ind*batch_size
        end_ind = (ind+1)*batch_size
        batch_X = X[start_ind:end_ind].toarray()
        batch_y = y[start_ind:end_ind]
        yield batch_X, batch_y


# In[123]:


from keras.models import Sequential, clone_model
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
from keras.metrics import binary_accuracy
from keras.callbacks import ModelCheckpoint, EarlyStopping


# In[ ]:


model = Sequential()
model.add(Dense(64, input_dim=100000))
model.add(Activation("relu"))
model.add(Dropout(0.2))
model.add(Dense(1))
model.add(Activation("sigmoid"))
model.compile(optimizer=Adam(),
              loss=binary_crossentropy,
              metrics=[binary_accuracy])


# In[28]:


cv_pipe_file = "lr4.pkl"
with open(cv_pipe_file, "rb") as f:
    cv_pipe = pickle.load(f, encoding="latin1")
cv = cv_pipe.steps[0][1]
cv_x_train = cv.transform(x_train)
cv_x_val = cv.transform(x_val)
cv_x_test = cv.transform(x_test)


# In[30]:


batch_size = 32
model.fit_generator(generator=batch_generator(cv_x_train, y_train, batch_size), 
                    epochs=1,
                    validation_data=(cv_x_val, y_val),
                    steps_per_epoch=cv_x_train.shape[0]/batch_size)


# In[22]:


d2v = Doc2Vec.load("d2v1.d2v")


# In[23]:


x = d2v[d2v.docvecs.index2entity]
y = small_y_all
x.shape


# In[24]:


# x = d2v[d2v.docvecs.index2entity]
# y = small_y_all
x_tr, x_vl, y_tr, y_vl = train_test_split(x, y, test_size=0.01, random_state=SEED)
# model = evaluate_and_get_model(x, y)


# In[27]:


# lr = LogisticRegression()
# lr.fit(x_tr, y_tr)
# lr.score(x_vl, y_vl)


# In[34]:


# d2v[["rame", "want"]]
d2v.wv.vocab.keys()


# # GloVe 

# In[17]:


import gensim.downloader as api


# In[25]:


twt = api.load("glove-twitter-50")


# In[50]:


twt.vocab
twt.vectors
twt["night"]


# In[65]:


info = api.info()
info["models"].keys()


# In[163]:


twt["this"]


# In[13]:


def sentence_to_arr(sentence, vectors, feature_num):
    words = sentence.split()
    result = np.zeros((len(words), feature_num))
    for ind, word in enumerate(words):
        if word in vectors:
            word_arr = vectors[word].reshape(1, feature_num)
            result[ind] = word_arr
    result = result[(result != 0).any(axis=1)]
    if result.size == 0:
        return np.array([np.nan] * feature_num)
    result = np.mean(result, axis=0)
    return result


# In[195]:


arr = sentence_to_arr("I want it", twt, 50)
arr


# In[354]:


tmp = np.array([[1,2,3], [4,5,6], [7,8,9]])


# In[18]:


def remove_indexes_from_arr(arr, indexes):
    mask = np.ones(arr.shape, dtype=bool)
    mask[indexes] = False
    return arr[mask]


# In[19]:


def get_x_y_from_vectors(vectors, feature_num, x, y):
    x_glove = np.array([sentence_to_arr(elem, vectors, feature_num) for elem in x])
    nan_indexes = np.unique(np.argwhere(np.isnan(x_glove))[:,0])
    x_glove = remove_indexes_from_arr(x_glove, nan_indexes).reshape(-1, feature_num)
    y_glove = remove_indexes_from_arr(y, nan_indexes)
    return x_glove, y_glove


# In[361]:


x_train_glove, y_train_glove = get_x_y_from_vectors(twt, 50, x_train, y_train)
x_val_glove, y_val_glove = get_x_y_from_vectors(twt, 50, x_val, y_val)


# In[373]:


lr = LogisticRegression()
lr.fit(x_train_glove, y_train_glove)
lr.score(x_val_glove, y_val_glove)


# # Word2Vec

# In[20]:


x_all.shape


# In[21]:


import multiprocessing

def get_word2vec_model(skip_gram=0):
    cpu_num = multiprocessing.cpu_count()
    feature_num = 100
    max_distance = 5
    noise_words = 5
    minimum_word_num = 5
    w2v_cbow = Word2Vec(sg=skip_gram, size=feature_num, workers=cpu_num, negative=noise_words, window=max_distance, 
                    min_count=minimum_word_num)
    return w2v_cbow


# In[22]:


from sklearn.utils import shuffle

def train(model, sentences):
    learning_rate = 0.05
    min_learning_rate = 0.01
    epoch_num = 10
    learning_rate_delta = (learning_rate - min_learning_rate) / (epoch_num - 1)
    current_learning_rate = learning_rate
    for epoch in range(epoch_num):
        current_sentences = shuffle(sentences)
        model.alpha, model.min_alpha = current_learning_rate, current_learning_rate
        model.train(current_sentences, total_examples=len(current_sentences), epochs=1)
        print("epoch {} completed with alpha {}".format(epoch, current_learning_rate))
        current_learning_rate -= learning_rate_delta


# In[32]:


w2v_cbow = get_word2vec_model(skip_gram=0)
w2v_data = small_x_all.apply(str.split)
w2v_cbow.build_vocab(w2v_data)
train(w2v_cbow, w2v_data)
w2v_x_train, w2v_y_train = get_x_y_from_vectors(w2v_cbow.wv, 100, small_x_train, small_y_train)
w2v_x_val, w2v_y_val = get_x_y_from_vectors(w2v_cbow.wv, 100, small_x_val, small_y_val)


# In[23]:


def evaluate_and_get_model(x_train, y_train, x_val, y_val):
    lr = LogisticRegression()
    lr.fit(x_train,  y_train)
    get_model_info(lr, x_val, y_val)
    return lr


# In[34]:


evaluate_and_get_model(w2v_x_train, w2v_y_train, w2v_x_val, w2v_y_val)


# In[58]:


model = Sequential()
model.add(Dense(64, activation="relu", input_dim=w2v_x_train.shape[1]))
model.add(Dense(128, activation="relu"))
model.add(Dense(1, activation="sigmoid"))
model.compile(optimizer="adam",
             loss="binary_crossentropy",
             metrics=["accuracy"])


# In[59]:


# file_path = "w2v_weights.{epoch:02d}-{val_acc:.4f}.hdf5"
# checkpoint = ModelCheckpoint(filepath=file_path, monitor="val_acc", verbose=1, save_best_only=True)
early_stop = EarlyStopping(monitor="val_acc", patience=5)


# In[60]:


model.fit(w2v_x_train, w2v_y_train,
         validation_data=(w2v_x_val, w2v_y_val),
         epochs=100, batch_size=32, verbose=2, callbacks=[early_stop])


# # Getting Ready for CNN

# In[27]:


import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))


# In[28]:


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences


# In[29]:


from gensim.models import KeyedVectors
w2v_cbow_vec = KeyedVectors.load("w2v_cbow.kv")
w2v_sg_vec = KeyedVectors.load("w2v_sg.kv")


# In[101]:


len(w2v_cbow_vec.wv.vocab.keys())


# In[136]:


def get_max_len(arr):
    return max(arr.apply(lambda a :len(a.split())))


# In[138]:


max_len = get_max_len(x_all) + 15


# In[139]:


tok = Tokenizer(num_words=100000)
tok.fit_on_texts(x_train)


# In[140]:


def get_sequences(tok, x_train, x_val):
    train_sequences = tok.texts_to_sequences(x_train)
    val_sequences = tok.texts_to_sequences(x_val)
    x_train_seq = pad_sequences(train_sequences, maxlen=max_len)
    x_val_seq = pad_sequences(val_sequences, maxlen=max_len)
    return x_train_seq, x_val_seq


# In[142]:


x_train_seq, x_val_seq = get_sequences(tok, x_train, x_val)
x_train_seq[:5]


# In[ ]:


def get_concat_vocab(): 
    w2v_cbow_vec = KeyedVectors.load("w2v_cbow.kv")
    w2v_sg_vec = KeyedVectors.load("w2v_sg.kv")
    concatenated_vocab = dict()
    for word in w2v_cbow_vec.wv.vocab.keys():
        word_as_arr = np.append(w2v_cbow_vec.wv[word], w2v_sg_vec.wv[word]) 
        concatenated_vocab[word] = word_as_arr
    return concatenated_vocab


# In[ ]:


concat_vocab = get_concat_vocab()


# In[ ]:


def get_weights(tok, concatenated_vocab, x_train, x_val):
    weights = np.zeros((100000, 200))
    for word, ind in tok.word_index.items():
        if ind > 100000:
            continue
        weights[ind - 1] = concatenated_vocab[word]
    return weights


# In[ ]:


weights = get_weights(tok, concat_vocab, x_train, x_val)


# In[113]:


np.array_equal(weights[965], conc_vocab["lets"])


# In[131]:


from keras.models import Sequential, clone_model
from keras.layers import Dense, Activation, Dropout, Flatten, Embedding
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
from keras.metrics import binary_accuracy
from keras.callbacks import ModelCheckpoint, EarlyStopping


# In[135]:


model1 = Sequential()
model1.add(Embedding(100000, 200, input_length=max_len))
model1.add(Flatten())
model1.add(Dense(32, activation="relu"))
model1.add(Dense(1, activation="sigmoid"))
model1.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])
model1.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), epochs=5, batch_size=32, verbose=2)

