import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

w2v_x_train = np.load("w2v_x_train.npy")
w2v_y_train = np.load("w2v_y_train.npy")
w2v_x_val = np.load("w2v_x_val.npy")
w2v_y_val = np.load("w2v_y_val.npy")

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))

from keras.models import Sequential, clone_model
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
from keras.metrics import binary_accuracy
from keras.callbacks import ModelCheckpoint, EarlyStopping

model = Sequential()
model.add(Dense(64, activation="relu", input_dim=w2v_x_train.shape[1]))
model.add(Dense(128, activation="relu"))
model.add(Dense(1, activation="sigmoid"))
model.compile(optimizer="adam",
             loss="binary_crossentropy",
             metrics=["accuracy"])

# file_path = "w2v_weights.{epoch:02d}-{val_acc:.4f}.hdf5"
# checkpoint = ModelCheckpoint(filepath=file_path, monitor="val_acc", verbose=1, save_best_only=True)
early_stop = EarlyStopping(monitor="val_acc", patience=7)

model.fit(w2v_x_train, w2v_y_train,
         validation_data=(w2v_x_val, w2v_y_val),
         epochs=100, batch_size=32, verbose=2, callbacks=[early_stop])