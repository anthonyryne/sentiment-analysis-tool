import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)
SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle

def get_model_info(model, x_test, y_test):
    y_predicted = model.predict(x_test)
    accuracy = accuracy_score(y_test, y_predicted)
    print("accuracy: {}".format(accuracy))
    conf = confusion_matrix(y_predicted, y_test)
    conf_df = pd.DataFrame(conf, index=["real 1", "real 0"], columns=["predicted 1", "predicted 0"])
    print("Confusion Matrix:")
    print(conf_df)
    tp, fp, fn, tn = conf.ravel()
    precision = tp/(tp+fp)
    recall = tp/(tp+fn)
    f1 = 2 * precision * recall / (precision + recall)
    print("precision: {}".format(precision))
    print("recall: {}".format(recall))
    print("f1: {}".format(f1))
    
from gensim.models import Doc2Vec, Word2Vec
from gensim.models.doc2vec import TaggedDocument
from sklearn.model_selection import train_test_split

# small_data = data[:100000]
small_data = data
import multiprocessing
cpu_num = multiprocessing.cpu_count()
small_x = small_data["text"]
small_y = small_data["sentiment"]
small_x_train, small_x_test, small_y_train, small_y_test = train_test_split(small_x, small_y, test_size=0.2, random_state=SEED)
small_x_val, small_x_test, small_y_val, small_y_test = train_test_split(small_x_test, small_y_test, test_size=0.5, random_state=SEED)

def get_tagged_data(data):
    return [TaggedDocument(elem.split(), ["ind{}".format(ind)]) for ind, elem in zip(data.index, data)]

from gensim.models.doc2vec import TaggedDocument
small_x_all = pd.concat([small_x_train, small_x_val, small_x_test], ignore_index=True)
small_y_all = pd.concat([small_y_train, small_y_val, small_y_test], ignore_index=True)
tgd = get_tagged_data(small_x_all)

features_num = 100
workers_num = cpu_num
minimum_word_num = 5
max_distance = 5
noise_words = 5
learning_rate = 0.05
min_learning_rate = 0.01
epochs_num = 5
learning_rate_delta = (learning_rate - min_learning_rate) / (epochs_num - 1)

from sklearn.utils import shuffle

def train(model):
    current_learning_rate = learning_rate
    for epoch in range(epochs_num):
        current_tgd = shuffle(tgd)
        model.alpha, model.min_alpha = current_learning_rate, current_learning_rate
        model.train(current_tgd, total_examples=len(current_tgd), epochs=1)
        print("epoch {} completed with alpha {}".format(epoch, current_learning_rate))
        current_learning_rate -= learning_rate_delta
        
def evaluate_and_get_model(x, y):
    lr = LogisticRegression()
    lr.fit(x, y)
    get_model_info(lr, x, y)
    return lr

d2v1 = Doc2Vec(dm=0, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
d2v2 = Doc2Vec(dm=1, dm_concat=1, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
d2v3 = Doc2Vec(dm=1, dm_mean=1, vector_size=features_num, workers=workers_num, min_count=minimum_word_num, 
               window=max_distance, negative=noise_words, epochs=epochs_num)
doc2vecs = [d2v1, d2v2, d2v3]
d2v = doc2vecs[0]

x_s = list()
for ind, d2v in enumerate(doc2vecs):
    d2v.build_vocab(tgd)
    train(d2v)
    d2v.save("d2v{}.d2v".format(ind + 1))
    x = d2v[d2v.docvecs.index2entity]
    x_s.append(x)
    y = small_y_all
    model = evaluate_and_get_model(x, y)
    with open("lr_d2v{}.pkl".format(ind + 1), "wb") as f:
        pickle.dump(model, f)

for i in range(2):
    x1 = x_s[0]
    x2 = x_s[i+1]
    new_shape = (x1.shape[0], x2.shape[1]*2)
    x = np.stack((x1, x2), axis=1).reshape(new_shape)
    x_s.append(x)
    model = evaluate_and_get_model(x, y)
    with open("lr_d2v{}.pkl".format(3 + i + 1), "wb") as f:
        pickle.dump(model, f)