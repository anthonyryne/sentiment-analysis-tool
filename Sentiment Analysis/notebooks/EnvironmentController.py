class LukeController:
    folder_name = "/var/neiron/ig/sa/data/"
    file_name = "big_data.csv"


class MyController:
    folder_name = "C:/Users/igugu/Desktop/amazon/"
    file_name = "small_data.csv"


class EnvironmentController:
    def __init__(self, current_controller):
        self.controller = current_controller

    def _file_name(self):
        return self.controller.file_name

    def _folder_name(self):
        return self.controller.folder_name

    def full_file_name(self):
        return self._folder_name() + self._file_name()
