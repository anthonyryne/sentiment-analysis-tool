
# coding: utf-8

# In[4]:


data_folder = "./word2vec-sentiments/"
import os
os.listdir(data_folder)


# In[2]:


import numpy as np
import pandas as pd


# In[26]:


train_pos_file = data_folder + "train-pos.txt"
train_neg_file = data_folder + "train-neg.txt"
test_pos_file = data_folder + "test-pos.txt"
test_neg_file = data_folder + "test-neg.txt"


# In[37]:


from gensim.utils import smart_open

def get_words_from_files(*file_names):
    for file_name in file_names:
        with smart_open(file_name) as fin:
            words = [elem.decode("utf-8") for elem in fin]
        yield words


# In[133]:


train_pos, train_neg, test_pos, test_neg = get_words_from_files(train_pos_file, train_neg_file, test_pos_file, test_neg_file)

pos_df = pd.DataFrame(train_pos + test_pos, columns=["text"])
pos_df["sentiment"] = 1
neg_df = pd.DataFrame(train_neg + test_neg, columns=["text"])
neg_df["sentiment"] = 0

data = pos_df.append(neg_df)
data = data.sample(frac=1).reset_index(drop=True)
print(max(data.index))


# In[156]:


data


# In[134]:


def get_tagged_data(data):
    return [TaggedDocument(elem.split(), ["ind{}".format(ind)]) for ind, elem in zip(data.index, data)]


# In[135]:


from gensim.models.doc2vec import TaggedDocument
tgd = get_tagged_data(data["text"])


# In[136]:


from gensim.models import Doc2Vec


# In[137]:


model = Doc2Vec(min_count=1, window=10, vector_size=100, sample=1e-4, negative=5, workers=8)
model.build_vocab(tgd)


# In[138]:


SEED = 1000
seed(SEED)

for epoch in range(10):
    model.train(shuffle(tgd), total_examples=len(tgd), epochs=1)


# In[139]:


from sklearn.model_selection import train_test_split
small_x = data["text"]
small_y = data["sentiment"]
small_x_train, small_x_test, small_y_train, small_y_test = train_test_split(small_x, small_y, test_size=0.2, random_state=SEED)
small_x_val, small_x_test, small_y_val, small_y_test = train_test_split(small_x_test, small_y_test, test_size=0.5, random_state=SEED)


# In[147]:


def get_train_val_test_vectors(vectors):
    x_train_vec_len = 40000
    x_val_vec_len = x_test_vec_len = 5000
    x_train_vec = vectors[0:x_train_vec_len]
    x_val_vec = vectors[x_train_vec_len : x_train_vec_len + x_val_vec_len]
    x_test_vec = vectors[x_train_vec_len + x_val_vec_len : x_train_vec_len + x_val_vec_len + x_val_vec_len]
    return x_train_vec, x_val_vec, x_test_vec


# In[155]:


results = [model["ind{}".format(i)] for i in range(50000)]
x_train_vec = results[0:40000]
x_val_vec = results[40000:45000]
x_test_vec = results[45000:50000]


# In[154]:


from sklearn.linear_model import LogisticRegression
lr = LogisticRegression()
lr.fit(x_train_vec, small_y_train)
print(lr.score(x_test_vec, small_y_test))


# In[172]:


a = lr.predict(x_train_vec)
r = 0
b = data["sentiment"].values


# In[173]:


for ind, elem in enumerate(a):
    elem2 = b[ind]
    if elem != elem2:
        r += 1
r


# In[174]:


len(a)

