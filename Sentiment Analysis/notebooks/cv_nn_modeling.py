import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)

SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
import pickle


cv_pipe_file = "lr4.pkl"
with open(cv_pipe_file, "rb") as f:
    cv_pipe = pickle.load(f, encoding="latin1")
cv = cv_pipe.steps[0][1]
cv_x_train = cv.transform(x_train)
cv_x_val = cv.transform(x_val)
cv_x_test = cv.transform(x_test)

SEED = 9
np.random.seed(SEED)

from keras.models import Sequential, clone_model
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
from keras.metrics import binary_accuracy


def batch_generator(X, y, batch_size=32, shuffle=False):
    sample_num = X.shape[0]
    batch_num = sample_num / batch_size
    indexes = np.arange(sample_num)
    if shuffle:
        np.random.shuffle(indexes)
    for ind in indexes:
        start_ind = ind*batch_size
        end_ind = (ind+1)*batch_size
        batch_X = X[start_ind:end_ind].toarray()
        batch_y = y[start_ind:end_ind]
        yield batch_X, batch_y

from sklearn.preprocessing import Normalizer
norm = Normalizer().fit(cv_x_train)
cv_x_train_norm = norm.transform(cv_x_train)
cv_x_val_norm = norm.transform(cv_x_val)


model1 = Sequential()
model1.add(Dense(64, input_dim=100000))
model1.add(Activation("relu"))
model1.add(Dense(1))
model1.add(Activation("sigmoid"))
model1.compile(optimizer=Adam(),
              loss=binary_crossentropy,
              metrics=[binary_accuracy])

model2 = Sequential()
model2.add(Dense(64, input_dim=100000))
model2.add(Activation("relu"))
model2.add(Dropout(0.2))
model2.add(Dense(1))
model2.add(Activation("sigmoid"))
model2.compile(optimizer=Adam(),
              loss=binary_crossentropy,
              metrics=[binary_accuracy])

model3 = Sequential()
model3.add(Dense(64, input_dim=100000))
model3.add(Activation("relu"))
model3.add(Dropout(0.2))
model3.add(Dense(128))
model3.add(Activation("relu"))
model3.add(Dropout(0.3))
model3.add(Dense(1))
model3.add(Activation("sigmoid"))
model3.compile(optimizer=Adam(),
              loss=binary_crossentropy,
              metrics=[binary_accuracy])

models = [model1, model1, model1 , clone_model(model1), clone_model(model1)]

batch_size = 32
epoch_num = 5

for ind, model in enumerate(models):
    val_data = (cv_x_val, y_val)
    if ind == 2:
        cur_batch_generator = batch_generator(cv_x_train_norm, y_train, batch_size)
        val_data = (cv_x_val_norm, y_val)
    elif ind == 1:
        cur_batch_generator = batch_generator(cv_x_train, y_train, batch_size, True)
    else:
        cur_batch_generator = batch_generator(cv_x_train, y_train, batch_size)
    model.fit_generator(generator=cur_batch_generator, 
                        epochs=epoch_num,
                        validation_data=val_data,
                        steps_per_epoch=cv_x_train.shape[0]/batch_size)


