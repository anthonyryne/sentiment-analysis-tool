
# coding: utf-8

# # Data Preprocessing

# In[1]:


import numpy as np
import pandas as pd


# In[10]:


train_data_file = r"C:\Users\igugu\Desktop\amazon\train.ft.txt"
test_data_file = r"C:\Users\igugu\Desktop\amazon\test.ft.txt"
# train_data = pd.read_table(train_data_file, header=None)
test_data = pd.read_table(test_data_file, header=None)
test_data.head()


# In[11]:


def preprocess_data(data):
    data["text"] = data.apply(lambda row: " ".join(row[0].split(" ")[1:]), axis=1)
    data["sentiment"] = data.apply(lambda row: row[0].split(" ")[0].split("__")[-1].strip(), axis=1)
    data = data.drop(columns=[0])
    return data


# In[12]:


# preprocess_data(train_data)
test_data = preprocess_data(test_data)


# In[13]:


print(test_data.shape)
test_data.head()


# # Modeling

# In[17]:


from keras.models import Sequential
from keras.layers import Dense, Activation

