
# coding: utf-8

# # Data Preprocessing

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import re
from keras.preprocessing.text import Tokenizer
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder


# In[2]:


df = pd.read_csv("tweets.csv")


# In[3]:


df = df[["text", "airline_sentiment"]]
df.columns = ["text", "sentiment"]
# df = df[df["sentiment"] != " neutral"]
df.head()


# In[4]:


def shuffle_df(df):
    # df = df.reindex(np.random.permutation(df.index))
    df = df.sample(frac=1)
    df = df.reset_index(drop=True)
    return df


# In[5]:


df = shuffle_df(df)


# In[6]:


def get_stopwords():
    my_stopwords = stopwords.words("english")
    my_stopwords = [elem for elem in my_stopwords if not(("not" in elem) or ("n't" in elem) or ("no" in elem))]
    return my_stopwords


# In[7]:


def remove_stopwords(df):
    stop_words = get_stopwords()
    df["text"] = df["text"].apply(lambda a: " ".join([elem for elem in a.split(" ") if elem not in stop_words]))


# In[8]:


remove_stopwords(df)


# In[9]:


def remove_ats(df):
    df["text"] = df["text"].apply(lambda a : re.sub(r"@\w+", "", a))


# In[53]:


remove_ats(df)


# In[54]:


from sklearn.model_selection import train_test_split
print(len(df))
train_x, test_x, train_y, test_y = train_test_split(df["text"],df["sentiment"], test_size=0.1)
print("we have {} train examples".format(train_x.shape[0]))
print("we have {} test examples".format(test_x.shape[0]))


# In[55]:


def train_test_x_to_number_sequences(tok, train_x, test_x, word_num):
    tok.fit_on_texts(train_x)
    train_seq_x = tok.texts_to_sequences(train_x)
    test_seq_x = tok.texts_to_sequences(test_x)
    yield train_seq_x
    yield test_seq_x


# In[56]:


WORD_NUM = 10000
EPOCH_NUM = 20
BATCH_SIZE = 256


# In[57]:


tok = Tokenizer(num_words=WORD_NUM,
                filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~ ', 
                lower=True, 
                split=' ')
train_seq_x, test_seq_x = train_test_x_to_number_sequences(tok, train_x, test_x, WORD_NUM)
test_seq_x = tok.texts_to_sequences(test_x)
print(train_seq_x[0])
print(train_x.iloc[0])
train_seq_x


# In[58]:


def array_to_one_hot(arr):
    one_hot_arr = np.zeros((len(arr), WORD_NUM))
    for ind, elem in enumerate(arr):
        one_hot_arr[ind, elem] = 1
    return one_hot_arr


# In[59]:


def train_test_one_hot(train_x, test_x, word_num=10000):
    train_seq_x, test_seq_x = train_test_x_to_number_sequences(tok, train_x, test_x, word_num)
    train_x_one_hot = array_to_one_hot(train_seq_x)
    test_x_one_hot = array_to_one_hot(test_seq_x)
    yield train_x_one_hot
    yield test_x_one_hot
    le = LabelEncoder()
    train_y_one_hot = to_categorical(le.fit_transform(train_y))
    test_y_one_hot = to_categorical(le.fit_transform(test_y))
    yield train_y_one_hot
    yield test_y_one_hot
    yield le


# In[60]:


train_x_one_hot, test_x_one_hot, train_y_one_hot, test_y_one_hot, le = train_test_one_hot(train_x, test_x, WORD_NUM)
train_x_one_hot.shape


# In[61]:


x_tr, x_val, y_tr, y_val = train_test_split(train_x_one_hot, train_y_one_hot, test_size=0.1)


# In[62]:


x_tr


# # MODELING

# In[63]:


from keras.models import Sequential
from keras.layers import Dense, Activation


# In[64]:


x_tr.shape


# In[65]:


model = Sequential()
model.add(Dense(64, activation="relu", input_shape=(WORD_NUM,)))
model.add(Dense(64, activation="relu"))
model.add(Dense(3, activation="softmax"))
model.summary()


# In[66]:


model.compile(optimizer="rmsprop",
              loss="categorical_crossentropy",
              metrics=["accuracy"])


# In[67]:


fitted_model = model.fit(x_tr, y_tr, epochs=EPOCH_NUM, batch_size=BATCH_SIZE, validation_data=(x_val, y_val))


# In[19]:


# score = model.evaluate(x_val, y_val, batch_size=BATCH_SIZE)
# score


# In[68]:


print(model.metrics_names)
print(fitted_model.history.keys())


# In[69]:


train_loss = fitted_model.history["loss"]
train_accuracy = fitted_model.history["acc"]
val_loss = fitted_model.history["val_loss"]
val_accuracy = fitted_model.history["val_acc"]


# In[70]:


plt.plot(train_loss, label="train loss", color="green")
plt.plot(val_loss, label="validation loss", color="blue")
plt.legend()
plt.show()


# In[71]:


plt.plot(train_accuracy, label="train accuracy", color="green")
plt.plot(val_accuracy, label="validation accuracy", color="blue")
plt.legend()
plt.show()


# Analyzing train/cross_valid accuracy/loss we can see that overfitting is killing us

# # Killing Overfitting

# In[72]:


from keras.layers import Dropout
from keras.regularizers import l1,l2


# In[73]:


model = Sequential()
model.add(Dense(64, activation="relu", kernel_regularizer=l2(0.005), input_shape=(WORD_NUM,)))
model.add(Dropout(0.5))
model.add(Dense(32, activation="relu", kernel_regularizer=l2(0.005)))
model.add(Dropout(0.5))
model.add(Dense(64, activation="relu", kernel_regularizer=l2(0.005)))
model.add(Dense(3, activation="softmax"))
model.summary()


# In[74]:


model.compile(optimizer="rmsprop",
              loss="categorical_crossentropy",
              metrics=["accuracy"])

fitted_model = model.fit(x_tr, y_tr, epochs=EPOCH_NUM, batch_size=BATCH_SIZE, validation_data=(x_val, y_val))

train_loss = fitted_model.history["loss"]
train_accuracy = fitted_model.history["acc"]
val_loss = fitted_model.history["val_loss"]
val_accuracy = fitted_model.history["val_acc"]

plt.plot(train_loss, label="train loss", color="green")
plt.plot(val_loss, label="validation loss", color="blue")
plt.legend()
plt.show()


# In[75]:


from keras.models import save_model
save_model(model, "SA.h5")


# In[76]:


from keras.models import load_model
new_model = load_model("SA.h5")


# In[91]:


text = input("")
text_tok = tok.texts_to_sequences([text])
text_oh = array_to_one_hot(text_tok)
prediction = new_model.predict(text_oh)
max_index = np.argmax(prediction)
# print(prediction[0][max_index])
# result = le.classes_[max_index]
print(prediction[0])
result = le.classes_
result


# # ANOTHER DATA

# In[113]:


train_data_file = r"C:\Users\igugu\Desktop\amazon\train.ft.txt"
test_data_file = r"C:\Users\igugu\Desktop\amazon\test.ft.txt"
train_data = pd.read_table(train_data_file, header=None)
test_data = pd.read_table(test_data_file, header=None)
test_data.head()


# In[86]:


def preprocess_data(data):
    data["text"] = data.apply(lambda row: " ".join(row[0].split(" ")[1:]), axis=1)
    data["sentiment"] = data.apply(lambda row: row[0].split(" ")[0].split("__")[-1].strip(), axis=1)
    data = data.drop(columns=[0])
    return data


# In[114]:


train_data = preprocess_data(train_data)
test_data = preprocess_data(test_data)


# In[115]:


train_data.shape


# In[ ]:


remove_stopwords(test_data)
train_x, test_x, train_y, test_y = train_test_split(test_data["text"],test_data["sentiment"], test_size=0.1)
train_x_one_hot, test_x_one_hot, train_y_one_hot, test_y_one_hot, _ = train_test_one_hot(train_x, test_x, WORD_NUM)
x_tr, x_val, y_tr, y_val = train_test_split(train_x_one_hot, train_y_one_hot, test_size=0.1)


# In[128]:


model = Sequential()
model.add(Dense(64, activation="relu", kernel_regularizer=l2(0.01), input_shape=(WORD_NUM,)))
model.add(Dropout(0.6))
model.add(Dense(32, activation="relu", kernel_regularizer=l2(0.01)))
model.add(Dropout(0.6))
model.add(Dense(64, activation="relu", kernel_regularizer=l2(0.01)))
model.add(Dense(3, activation="softmax"))
model.summary()


# In[129]:


BATCH_SIZE = 1024
EPOCH_NUM = 40
model.compile(optimizer="rmsprop",
              loss="categorical_crossentropy",
              metrics=["accuracy"])

fitted_model = model.fit(x_tr, y_tr, epochs=EPOCH_NUM, batch_size=BATCH_SIZE, validation_data=(x_val, y_val))

train_loss = fitted_model.history["loss"]
train_accuracy = fitted_model.history["acc"]
val_loss = fitted_model.history["val_loss"]
val_accuracy = fitted_model.history["val_acc"]

plt.plot(train_loss, label="train loss", color="green")
plt.plot(val_loss, label="validation loss", color="blue")
plt.legend()
plt.show()

# getting ready for big changes :D