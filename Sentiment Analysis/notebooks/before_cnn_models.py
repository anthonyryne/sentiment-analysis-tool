import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))

import numpy as np
import pandas as pd

print("reading data")
big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)

print("sampling data")
SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

print("splitting data")
from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)

x_all = pd.concat([x_train, x_val, x_test], ignore_index=True)
y_all = pd.concat([y_train, y_val, y_test], ignore_index=True)


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

print("getting maximum lengh")
def get_max_len(arr):
    return max(arr.apply(lambda a :len(a.split())))

max_len = get_max_len(x_all) + 15

print("tokenizing and fitting")
tok = Tokenizer(num_words=100000)
tok.fit_on_texts(x_train)

def get_sequences(tok, x_train, x_val):
    train_sequences = tok.texts_to_sequences(x_train)
    val_sequences = tok.texts_to_sequences(x_val)
    x_train_seq = pad_sequences(train_sequences, maxlen=max_len)
    x_val_seq = pad_sequences(val_sequences, maxlen=max_len)
    return x_train_seq, x_val_seq

print("getting sequences")
x_train_seq, x_val_seq = get_sequences(tok, x_train, x_val)

from gensim.models import KeyedVectors

def get_concat_vocab(): 
    w2v_cbow_vec = KeyedVectors.load("w2v_cbow.kv")
    w2v_sg_vec = KeyedVectors.load("w2v_sg.kv")
    concatenated_vocab = dict()
    for word in w2v_cbow_vec.wv.vocab.keys():
        word_as_arr = np.append(w2v_cbow_vec.wv[word], w2v_sg_vec.wv[word]) 
        concatenated_vocab[word] = word_as_arr
    return concatenated_vocab

print("getting concatenated vocabulary")
concat_vocab = get_concat_vocab()

def get_weights(tok, concatenated_vocab, x_train, x_val):
    weights = np.zeros((100000, 200))
    for word, ind in tok.word_index.items():
        if ind > 100000:
            continue
        weights[ind - 1] = concatenated_vocab[word]
    return weights

print("getting weights")
weights = get_weights(tok, concat_vocab, x_train, x_val)

from keras.models import Sequential, clone_model
from keras.layers import Dense, Activation, Dropout, Flatten, Embedding, Conv1D, GlobalMaxPool1D
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
from keras.metrics import binary_accuracy
from keras.callbacks import ModelCheckpoint, EarlyStopping

print("modeling with weights")
model1 = Sequential()
model1.add(Embedding(100000, 200, weights=[weights], input_length=max_len, trainable=True))
model1.add(Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu"))
model1.add(GlobalMaxPool1D())
model1.add(Dense(32, activation="relu"))
model1.add(Dense(1, activation="sigmoid"))
model1.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])
model1.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), epochs=5, batch_size=32, verbose=2)

print("modeling without weights")
model2 = Sequential()
model2.add(Embedding(100000, 200, input_length=max_len))
model2.add(Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu"))
model2.add(GlobalMaxPool1D())
model2.add(Dense(32, activation="relu"))
model2.add(Dense(1, activation="sigmoid"))
model2.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])
model2.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), epochs=5, batch_size=32, verbose=2)