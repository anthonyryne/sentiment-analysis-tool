
# coding: utf-8

# In[1]:


import sys
print(sys.executable)
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')


# In[2]:


import pandas as pd
import numpy as np


# # Data Read

# In[3]:


data = pd.read_csv("./data/big_data.csv")
data = data[:300000]
data.head()


# # Data Shuffle

# In[152]:


"%03d"  % 4


# In[138]:


from sklearn.utils import shuffle
seed = 1000
data = shuffle(data, random_state=seed).reset_index(drop=True)
data.head()


# # Data Split

# In[5]:


from sklearn.model_selection import train_test_split
x_train, x_val, y_train, y_val = train_test_split(data["text"], data["sentiment"], test_size=0.02, random_state=seed)
x_val, x_test, y_val, y_test = train_test_split(x_val, y_val, test_size=0.5, random_state=seed)
x_train.head()


# In[6]:


x_all = pd.concat([x_train, x_val, x_test])
y_all = pd.concat([y_train, y_val, y_test])
x_all.head()


# # Make Keras Not To Use All GPU Memory

# In[7]:


import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))


# # Preprocessing, Data Transform

# In[8]:


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences


# In[9]:


tok = Tokenizer(num_words=100000)
tok.fit_on_texts(x_train)


# In[10]:


def get_max_len(arr):
    return max(arr.apply(lambda a :len(a.split())))


# In[11]:


def get_sequences(tok, x_train, x_val, max_len):
    train_sequences = tok.texts_to_sequences(x_train)
    val_sequences = tok.texts_to_sequences(x_val)
    x_train_seq = pad_sequences(train_sequences, maxlen=max_len)
    x_val_seq = pad_sequences(val_sequences, maxlen=max_len)
    return x_train_seq, x_val_seq


# In[15]:


max_len = get_max_len(x_all) + 15
x_train_seq, x_val_seq = get_sequences(tok, x_train, x_val, max_len)
x_train_seq.shape


# In[16]:


from gensim.models import KeyedVectors


# In[17]:


def get_concat_vocab(): 
    w2v_cbow_vec = KeyedVectors.load("w2v_cbow.kv")
    w2v_sg_vec = KeyedVectors.load("w2v_sg.kv")
    concatenated_vocab = dict()
    for word in w2v_cbow_vec.wv.vocab.keys():
        word_as_arr = np.append(w2v_cbow_vec.wv[word], w2v_sg_vec.wv[word]) 
        concatenated_vocab[word] = word_as_arr
    return concatenated_vocab


# In[18]:


concat_vocab = get_concat_vocab()


# In[23]:


def get_weights(tok, concatenated_vocab, x_train, x_val):
    weights = np.zeros((100000, 200))
    for word, ind in tok.word_index.items():
        if ind > 100000:
            continue
        if word in concatenated_vocab:
            weights[ind - 1] = concatenated_vocab[word]
    return weights


# In[24]:


weights = get_weights(tok, concat_vocab, x_train, x_val)


# # Modeling

# In[27]:


from keras.models import Sequential
from keras.layers import Embedding, Conv1D, GlobalMaxPool1D, MaxPool1D


# In[41]:


model = Sequential()
model.add(Embedding(100000, 200, input_length=max_len))
model.add(Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu"))
model.add(GlobalMaxPool1D())
model.summary()


# In[60]:


from keras.models import Model
from keras.layers import Embedding, Conv1D, GlobalMaxPool1D
from keras.layers import Input, concatenate, Dropout, Dense


# In[58]:


x_train.shape


# In[66]:


data_input = Input((max_len,))
encoder = Embedding(100000, 200, weights=[weights], input_length=max_len, trainable=True)(data_input)
bigram = Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu")(encoder)
bigram = GlobalMaxPool1D()(bigram)
trigram = Conv1D(filters=100, kernel_size=3, strides=1, padding="valid", activation="relu")(encoder)
trigram = GlobalMaxPool1D()(trigram)
fourgram = Conv1D(filters=100, kernel_size=4, strides=1, padding="valid", activation="relu")(encoder)
fourgram = GlobalMaxPool1D()(fourgram)
grams = concatenate([bigram, trigram, fourgram], axis=1)

dense = Dense(128, activation="relu")(grams)
dropout = Dropout(0.2)(dense)
output = Dense(1, activation="sigmoid")(dropout)

model = Model(inputs=[data_input], outputs=[output])
model.compile(optimizer="adam", 
              loss="binary_crossentropy", 
              metrics=["accuracy"])
model.summary()


# In[69]:


model.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), batch_size=32, epochs=5, verbose=2)

