import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

big_data_file = "./data/big_data.csv"
small_data_file = "./data/small_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)

SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)

from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
    
import gensim.downloader as api

def sentence_to_arr(sentence, vectors, feature_num):
    words = sentence.split()
    result = np.zeros((len(words), feature_num))
    for ind, word in enumerate(words):
        if word in vectors:
            word_arr = vectors[word].reshape(1, feature_num)
            result[ind] = word_arr
    result = result[(result != 0).any(axis=1)]
    if result.size == 0:
        return np.array([np.nan] * feature_num)
    result = np.mean(result, axis=0)
    return result

def remove_indexes_from_arr(arr, indexes):
    mask = np.ones(arr.shape, dtype=bool)
    mask[indexes] = False
    return arr[mask]

def get_glove_x_y(vectors, x, y, feature_num):
    x_glove = np.array([sentence_to_arr(elem, vectors, feature_num) for elem in x])
    nan_indexes = np.unique(np.argwhere(np.isnan(x_glove))[:,0])
    x_glove = remove_indexes_from_arr(x_glove, nan_indexes).reshape(-1, feature_num)
    y_glove = remove_indexes_from_arr(y, nan_indexes)
    return x_glove, y_glove

api_texts = ["glove-twitter-200", "word2vec-google-news-300", "glove-wiki-gigaword-300"]
datas = [api.load(elem) for elem in api_texts]
feature_nums = [200, 300, 300]
for data, feature_num in zip(datas, feature_nums):
    x_train_glove, y_train_glove = get_glove_x_y(data, x_train, y_train, feature_num)
    x_val_glove, y_val_glove = get_glove_x_y(data, x_val, y_val, feature_num)
    lr = LogisticRegression()
    lr.fit(x_train_glove, y_train_glove)
    print(lr.score(x_val_glove, y_val_glove))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~")
    