import numpy as np
import pandas as pd
big_data_file = "./data/big_data.csv"
data_file = big_data_file
data = pd.read_csv(data_file)
SEED = 1000
data = data.sample(frac=1, random_state=SEED).reset_index(drop=True)
from sklearn.model_selection import train_test_split
x = data["text"]
y = data["sentiment"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=SEED)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=SEED)
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle
cv1 = CountVectorizer(max_features=100000)
cv2 = CountVectorizer(max_features=800000)
cv3 = CountVectorizer(max_features=100000, stop_words="english")
cv4 = CountVectorizer(max_features=100000, ngram_range=(1, 2))
cv5 = CountVectorizer(max_features=100000, ngram_range=(1, 3))
tvec1 = TfidfVectorizer(max_features=100000)
tvec2 = TfidfVectorizer(max_features=100000, ngram_range=(1,2))
tvec3 = TfidfVectorizer(max_features=100000, ngram_range=(1,3))
vectorizers = [cv1, cv2, cv3, cv4, cv5, tvec1, tvec2, tvec3]
file_names = ["lr{}.pkl".format(ind + 1) for ind in range(len(vectorizers))]
for ind, cv in enumerate(vectorizers):
    if ind <= 3:
        continue
    print("starting with index{}".format(ind))
    pipeline = Pipeline([("cv", cv), ("lr", LogisticRegression())])
    pipe = pipeline.fit(x_train, y_train)
    with open(file_names[ind], "wb") as f:
        pickle.dump(pipe, f)
    print("done with index {}".format(ind))