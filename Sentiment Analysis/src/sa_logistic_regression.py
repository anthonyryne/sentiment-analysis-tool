from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline


class SaLogisticRegression:
    """ Count Vectorizer Plus Logistic Regression for Sentiment Analysis"""

    def __init__(self, feature_num=100000):
        """Inits pipeline with CV and LR"""
        pipeline = Pipeline([("cv", CountVectorizer(max_features=feature_num, ngram_range=(1, 2))),
                             ("lr", LogisticRegression())])
        self.pipeline = pipeline

    def fit(self, x_train, y_train):
        """just FIT"""
        return self.pipeline.fit(x_train, y_train)

    def predict(self, x_test):
        """just PREDICT"""
        return self.pipeline.predict(x_test)

    def score(self, x_test, y_test):
        """just SCORE"""
        return self.pipeline.score(x_test, y_test)
