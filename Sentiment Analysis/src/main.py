import pandas as pd
from preprocessor import Preprocessor
from configer import get_config
from sa_logistic_regression import SaLogisticRegression
from utils import evaluate_model
from datar import Datar
from sa_word2vec import SaWord2vec
from sa_model import SaModel
import sys
import pickle

PREPROCESSING_DONE = True
BASELINE_MODEL_FITTED = True
DO_BASELINE = False
WORD2VEC_DONE = True
MODEL_FITTED = True


def main():
    """PREPROCESSING"""
    if len(sys.argv) > 1 and sys.argv[1].lower() == "default":
        config_key = "DEFAULT"
    else:
        config_key = "TEST"
    config = get_config(config_key)
    if not PREPROCESSING_DONE:
        preprocessor = Preprocessor(config)
        preprocessor.do_formatting()
        preprocessor.do_preprocessing()

    """DATA"""
    data = Datar(config)
    data.set_all_datas(True)

    """MODELING BASELINE"""
    feature_num = 100000 if config != "TEST" else 20000
    if DO_BASELINE:
        if not BASELINE_MODEL_FITTED:
            lr = SaLogisticRegression(feature_num=feature_num)
            lr.fit(data.x_train, data.y_train)

            with open(config["models_folder"] + "lr.pkl", "wb") as f:
                pickle.dump(lr, f)

        with open(config["models_folder"] + "lr.pkl", "rb") as f:
            lr = pickle.load(f)
            print(lr.score(data.x_train, data.y_train))
            print(lr.score(data.x_val, data.y_val))
        evaluate_model(lr, data.x_test, data.y_test)

    """MODELING"""
    if not WORD2VEC_DONE:
        w2v_data = data.x_all.apply(str.split)
        w2v_cbow = SaWord2vec(skip_gram=0)
        w2v_sg = SaWord2vec(skip_gram=1)
        sa_w2v_models = [w2v_cbow, w2v_sg]
        for ind, sa_w2v_model in enumerate(sa_w2v_models):
            sa_w2v_model.build_vocab(w2v_data)
            w2v_model = sa_w2v_model.train(w2v_data)
            w2v_model.save("{}w2v_{}.kv".format(config["working_process_data_folder"], "cbow" if ind == 0 else "sg"))

    sa_model = SaModel(config, data, feature_num, MODEL_FITTED)
    if not MODEL_FITTED:
        sa_model.fit()
    """lets test model on our inputs"""
    while 1:
        inputt = input()
        sentiment = sa_model.get_sentiment(inputt)
        print("printing sentiment")
        print(sentiment)


if __name__ == "__main__":
    main()
