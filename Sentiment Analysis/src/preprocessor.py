import numpy as np
import pandas as pd
import bz2
from configer import get_config
import os
import re


class Preprocessor:
    """Is responsible for data formatting and preprocessing"""

    data_file_names = ["data_formatted.csv", "data_preprocessed.csv"]

    def __init__(self, conf):
        """Reads config and sets data folder paths"""
        self.config = conf
        self.initial_data_folder = conf["initial_data_folder"]
        self.working_process_data_folder = conf["working_process_data_folder"]

    def do_preprocessing(self):
        """Does all data preprocessing"""
        row_number = 50000 if "test" in self.config["initial_data_folder"] else None
        formatted_data = pd.read_csv(self.initial_data_folder + self.data_file_names[0], nrows=row_number)
        preprocessed_data = self._get_preprocessed_data(formatted_data)
        preprocessed_data.to_csv(self.initial_data_folder + self.data_file_names[1], index=False)
        return preprocessed_data

    def _get_preprocessed_data(self, formatted_data):
        # parses formatted data and returns preprocessed, clean data
        formatted_data["text"] = formatted_data["text"].apply(lambda sentence: self._parse_sentence(sentence))
        formatted_data["sentiment"] = formatted_data["sentiment"].apply(lambda x: 0 if x == 1 else 1)
        return formatted_data

    def _parse_sentence(self, sentence):
        # replaces "n't" like words with "not" and also applies _parse_word method on every word of sentence
        parse_no_dict = {"won't": "will not", "can't": "can not", "n't": " not"}
        for k, v in parse_no_dict.items():
            sentence = sentence.replace(k, v)
        return " ".join([self._parse_word(word) for word in sentence.split(" ") if self._include_word(word)])

    @staticmethod
    def _parse_word(word):
        # removes any non-letter characters and makes everything lowercase
        word = re.sub("[^a-z A-Z]", "", word.lower().strip())
        return word

    @staticmethod
    def _include_word(word):
        # returns true if word is not a forbidden word
        forbidden_words = ["http", "www"]
        if word in forbidden_words:
            return False
        return True

    def do_formatting(self):
        """Does all data formatting"""
        formatted_data = self._get_formatted_data()
        formatted_data.to_csv(self.initial_data_folder + self.data_file_names[0], index=False)
        return formatted_data

    def _get_formatted_data(self):
        # Reads data, formats and returns it
        file_names = [elem for elem in os.listdir(self.initial_data_folder) if elem not in self.data_file_names]
        lines = list()
        for file_name in file_names:
            with bz2.open(self.initial_data_folder + file_name) as f:
                lines += f.readlines()
        data = pd.DataFrame(lines, columns=["full_text"])
        data["full_text"] = data["full_text"].apply(lambda x: x.decode("utf-8").strip())
        data["text"] = data["full_text"].apply(lambda x: " ".join(x.split(" ")[1:]))
        data["sentiment"] = data["full_text"].apply(lambda x: x.split(" ")[0][-1])
        data = data.drop(columns=["full_text"])
        return data


if __name__ == "__main__":
    print("preprocessor main")
    config_key = "TEST"
    config = get_config(config_key)
    preprocessor = Preprocessor(config)
    formatted = preprocessor.do_formatting()
    preprocessed = preprocessor.do_preprocessing()
    print(preprocessed.shape)
    print(preprocessed.head())
