import configparser

__doc__ = """ Is responsible for creating configuration file data adding and retrieving"""


def create_config():
    """Creates config file and fills it with needed parameters"""
    config = configparser.ConfigParser()
    data = {"initial_data_folder": "../data/real data/initial/",
            "working_process_data_folder": "../data/real data/working process/",
            "models_folder": "../models/real models/"}
    test_data = {"initial_data_folder": "../data/test data/initial/",
                 "working_process_data_folder": "../data/test data/working process/",
                 "models_folder": "../models/test models"}
    config["DEFAULT"] = data
    config["TEST"] = test_data
    with open("../config.ini", "w") as config_file:
        config.write(config_file)


def get_config(config_key="DEFAULT"):
    """Gets config file, parses and returns info as dictionary"""
    config = configparser.ConfigParser()
    config.read("../config.ini")
    return dict(config[config_key])


if __name__ == "__main__":
    print("configer main")
    create_config()
    conf = get_config()
    print(conf)
