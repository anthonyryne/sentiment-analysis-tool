import pandas as pd
from sklearn.model_selection import train_test_split


class Datar:
    """Is responsible for all the data we have, with splits"""

    def __init__(self, conf, seed=1000):
        """reads preprocessed data, samples and saves it"""
        data = pd.read_csv(conf["initial_data_folder"] + "data_preprocessed.csv")
        data = data.sample(frac=1, random_state=seed).reset_index(drop=True)
        self.data = data
        self.seed = seed
        self.x_train = None
        self.y_train = None
        self.x_val = None
        self.y_val = None
        self.x_test = None
        self.y_test = None
        self.x_all = None
        self.y_all = None

    def set_all_datas(self, verbose=False):
        """sets all splits for data"""
        x = self.data["text"]
        y = self.data["sentiment"]
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.02, random_state=self.seed)
        x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=self.seed)
        x_all = pd.concat([x_train, x_val, x_test])
        y_all = pd.concat([y_train, y_val, y_test])
        self.x_train = x_train
        self.y_train = y_train
        self.x_val = x_val
        self.y_val = y_val
        self.x_test = x_test
        self.y_test = y_test
        self.x_all = x_all
        self.y_all = y_all
        if verbose:
            train_positive_percent = round(len(x_train[y_train == 1]) / len(x_train), 3)
            val_positive_percent = round(len(x_val[y_val == 1]) / len(x_val), 3)
            test_positive_percent = round(len(x_test[y_test == 1]) / len(x_test), 3)
            print("train s contains {} examples where {} is positive".format(x_train.shape[0], train_positive_percent))
            print("validation s contains {} examples where {} is positive".format(x_val.shape[0], val_positive_percent))
            print("test s contains {} examples where {} is positive".format(x_test.shape[0], test_positive_percent))
