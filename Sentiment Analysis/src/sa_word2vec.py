from gensim.models import Word2Vec
import multiprocessing
from sklearn.utils import shuffle


class SaWord2vec:
    """ Word2Vec wrapper for sentiment analysis"""

    def __init__(self, skip_gram=0):
        """Setting up parameters for word2vec"""
        self.sg = 0
        cpu_num = multiprocessing.cpu_count()
        feature_num = 100
        max_distance = 5
        noise_words = 5
        minimum_word_num = 5
        model = Word2Vec(sg=skip_gram, size=feature_num, workers=cpu_num, negative=noise_words, window=max_distance,
                         min_count=minimum_word_num)
        self.model = model

    def build_vocab(self, w2v_data):
        """just build_vocab"""
        self.model.build_vocab(w2v_data)

    def train(self, sentences):
        """Whole training process here, returns word2vec model"""
        learning_rate = 0.05
        min_learning_rate = 0.01
        epoch_num = 10
        learning_rate_delta = (learning_rate - min_learning_rate) / (epoch_num - 1)
        current_learning_rate = learning_rate
        for epoch in range(epoch_num):
            current_sentences = shuffle(sentences)
            self.model.alpha, self.model.min_alpha = current_learning_rate, current_learning_rate
            self.model.train(current_sentences, epochs=1, total_examples=len(current_sentences))
            current_learning_rate -= learning_rate_delta
            print("epoch {} passed".format(epoch))
        return self.model
