import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model
from keras.layers import Embedding, Conv1D, GlobalMaxPool1D
from keras.layers import Input, concatenate, Dropout, Dense
from keras.callbacks import ModelCheckpoint
from gensim.models import KeyedVectors
from keras.models import load_model


class SaModel:
    """Our Main CNN model with all data preprocessings"""

    def __init__(self, conf, data, num_words=100000, model_fitted=False):
        """Sets tokenizer, data, and max_len"""
        if num_words == 100000:
            self._make_keras_not_use_all_memory()
        self.config = conf
        self.data = data
        self.num_words = num_words
        self.tok = Tokenizer(num_words=num_words)
        self.tok.fit_on_texts(data.x_train)
        self.max_len = self._get_max_len()
        self.x_train_seq, self.x_val_seq, self.x_test_seq = self._get_sequences()
        if not model_fitted:
            self.weights = self._get_weights()
            self.model = self._get_model()

    def get_sentiment(self, sentence):
        """Returns Sentiment on exchange to sentence"""
        sentence = np.array([sentence])
        loaded_model = load_model(self.config["models_folder"] + "CNN.hdf5")
        sentence_sequence = self._get_sequence(sentence)
        return loaded_model.predict(sentence_sequence)

    def fit(self):
        """Just fit plus evaluation on test set"""
        folder = self.config["models_folder"]
        file_path = folder + "cnn_weights.{epoch:02d}-{val_acc:.4f}.hdf5"
        checkpoint = ModelCheckpoint(file_path, monitor="val_acc", save_best_only=True, verbose=1)
        self.model.fit(self.x_train_seq, self.data.y_train, validation_data=(self.x_val_seq, self.data.y_val),
                       batch_size=32, epochs=5, verbose=2, callbacks=[checkpoint])
        print(self.model.evaluate(self.x_test_seq, self.data.y_test))

    def _get_model(self):
        # Returns model with CNN architecture
        data_input = Input((self.max_len,))
        encoder = Embedding(self.num_words, 200, weights=[self.weights], input_length=self.max_len, trainable=True)(data_input)
        bigram = Conv1D(filters=100, kernel_size=2, strides=1, padding="valid", activation="relu")(encoder)
        bigram = GlobalMaxPool1D()(bigram)
        trigram = Conv1D(filters=100, kernel_size=3, strides=1, padding="valid", activation="relu")(encoder)
        trigram = GlobalMaxPool1D()(trigram)
        fourgram = Conv1D(filters=100, kernel_size=4, strides=1, padding="valid", activation="relu")(encoder)
        fourgram = GlobalMaxPool1D()(fourgram)
        grams = concatenate([bigram, trigram, fourgram], axis=1)

        dense = Dense(256, activation="relu")(grams)
        dropout = Dropout(0.2)(dense)
        output = Dense(1, activation="sigmoid")(dropout)

        model = Model(inputs=[data_input], outputs=[output])
        model.compile(optimizer="adam",
                      loss="binary_crossentropy",
                      metrics=["accuracy"])

        model.summary()
        return model

    def _get_weights(self):
        # Returns weights
        concatenated_vocab = self._get_concat_vocab()
        weights = np.zeros((self.num_words, 200))
        for word, ind in self.tok.word_index.items():
            if ind > self.num_words:
                continue
            if word in concatenated_vocab:
                weights[ind - 1] = concatenated_vocab[word]
        return weights

    def _get_concat_vocab(self):
        # Returns concatenated vocab of sg and cbow word2vec vectors
        folder = self.config["working_process_data_folder"]
        w2v_cbow_vec = KeyedVectors.load(folder + "w2v_cbow.kv")
        w2v_sg_vec = KeyedVectors.load(folder + "w2v_sg.kv")
        concatenated_vocab = dict()
        for word in w2v_cbow_vec.wv.vocab.keys():
            word_as_arr = np.append(w2v_cbow_vec.wv[word], w2v_sg_vec.wv[word])
            concatenated_vocab[word] = word_as_arr
        return concatenated_vocab

    def _get_max_len(self):
        # Returns maximum number of words in sentence, almost :D
        return max(self.data.x_all.apply(lambda a: len(a.split()))) + 15

    def _get_sequences(self):
        # Transforms data splits into sequences with paddings
        return self._get_sequence(self.data.x_train, self.data.x_val, self.data.x_test)

    def _get_sequence(self, *arrays):
        # Returns padded sequences
        return [pad_sequences(self.tok.texts_to_sequences(arr), maxlen=self.max_len) for arr in arrays]

    @staticmethod
    def _make_keras_not_use_all_memory():
        # Prevent keras with backend tensorflow to use all gpu memory
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        set_session(tf.Session(config=config))
