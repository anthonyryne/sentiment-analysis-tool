import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

__doc__ = """This module saves all helper functions that doesn't belong to some other concrete module"""


def evaluate_model(model, x_test, y_test):
    """provides accuracy, precision, recall, f1 scores for model on test data"""
    y_predicted = model.predict(x_test)
    accuracy = accuracy_score(y_test, y_predicted)
    print("Accuracy: {}".format(accuracy))
    conf = confusion_matrix(y_test, y_predicted)
    conf_df = pd.DataFrame(conf, index=["real 1", "real 0"], columns=["predicted 1", "real 0"])
    print("Confusion Matrix")
    print(conf_df)
    tp, fp, fn, tn = conf.ravel()
    precision = tp/(tp+fp)
    recall = tp/(tp+fn)
    f1 = 2 * precision * recall / (precision + recall)
    print("Precision: {}".format(precision))
    print("Recall: {}".format(recall))
    print("F1 score: {}".format(f1))
