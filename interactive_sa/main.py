""" Main File """

import sys
sys.path.insert(0, '../')
from interactive_sa import data
from interactive_sa import constants as cnst
from interactive_sa.model import Model


def main():
    """ Main Function """

    print('\nHello and Welcome to Sentiment Analyzer Platform!')
    print('Let me first import data and build a model :)\n')
    # Process data
    data.process(load_data=True,
                 csv=cnst.FILE_TWITTER_TRAIN_DATA,
                 csv_clean=cnst.FILE_TWITTER_CLEANED_DATA)
    # Build and evaluate a mode
    # For other models check the MODEL IDs region in the constants.py file
    model = Model(cnst.MODEL_CNN_02, cnst.FILE_TWITTER_CLEANED_DATA, True)
    is_model_created = model.create()

    if is_model_created:
        # Test a model
        print('Starting Interactive Sentiment Analyzer...\n\n')
        model.analyzer()
    else:
        print('Sorry, something went wrong while creating a model.')

    # Exiting app
    print('Goodbye!')


if __name__ == "__main__":
    """ Main Entry Point """
    main()
