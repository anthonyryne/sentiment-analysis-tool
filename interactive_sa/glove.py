""" This file contains GloVe related functions """

import numpy as np
from interactive_sa import constants as cnst


def get_embeddings(file=None):
    """ Gets embeddings from the glove file"""

    # Check if argument is passed, if not, use a default file
    if file is None:
        file = cnst.FILE_GLOVE

    embeddings = {}

    # Read coefs from the file
    with open(file, 'r') as f:
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings[word] = coefs

    return embeddings
