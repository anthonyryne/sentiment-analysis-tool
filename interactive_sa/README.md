

# Welcome to The Tweet Sentiment Analyzer Platform!

This project is created to try to analyze a sentiment of tweets. 

This platform can:
 - Import and process data
 - Create different models
 - Analyze tweets
 
----------
## Getting Started
### Prerequisites

 - [A computer or a Server](https://amazon.com) (why not?)
 - [Python Version 3.6](https://www.python.org/downloads/) (for running Python scripts)
 - [Pandas](https://pypi.python.org/pypi/pandas) (for working with Data)
 - [NumPy](https://pypi.org/project/numpy/) (for math related functions)
 - [TensorFlow](https://pypi.org/project/tensorflow/) (for numerical computations)
 - [Keras](https://pypi.org/project/Keras/) (for NNs)
 - [Matplotlib](https://pypi.org/project/matplotlib/) (for graphs)
 - [SciKit-Learn](http://scikit-learn.org/stable/install.html) (for data processing)
 - [BeautifulSoup](https://pypi.org/project/bs4/) (for data cleaning)
 - [LXML](https://pypi.org/project/lxml/) (for bs4 lxml support)
 - [WordCloud](https://pypi.org/project/wordcloud/) (for nice word graphs)
 - [PyGubu](https://pypi.python.org/pypi/pygubu) (for building a Python GUI Design)
 - [Tkinter](https://wiki.python.org/moin/TkInter) (for Python GUI)



## Links
Models: https://drive.google.com/open?id=1Ik-MuHlwDRPyt0ma9cI17uGcRFaUeDwg



## Other
Please don't try to crush this app, because it will! :D 

