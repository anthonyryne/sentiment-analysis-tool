""" This file is used to load and process tweets """

import pandas as pd
import matplotlib.pyplot as plt
import re
import collections
import numpy as np
from interactive_sa import constants as cnst
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from wordcloud import WordCloud
from bs4 import BeautifulSoup
from numpy.random import RandomState
from interactive_sa.toolbox import print_progress_bar

# As there's no header in the dataset of tweets we will create it manually
# The csv file we import has the following columns
# 0 - the polarity of the tweet (0 = negative, 2 = neutral, 4 = positive)
# 1 - the id of the tweet (2087)
# 2 - the date of the tweet (Sat May 16 23:58:44 UTC 2009)
# 3 - the query (lyx). If there is no query, then this value is NO_QUERY.
# 4 - the user that tweeted (robotickilldozr)
# 5 - the text of the tweet (Lyx is cool)

# Names of the columns
data_cols = ['sentiment', 'id', 'date', 'query', 'user', 'text']
# Names of the columns to drop from the Dataset (we need only 'sentiment' and 'text' columns)
data_cols_to_drop = ['id', 'date', 'query', 'user']


def process(load_data, csv=cnst.FILE_TWITTER_TRAIN_DATA, csv_clean=cnst.FILE_TWITTER_CLEANED_DATA):
    """ Main Function """

    print('Starting data pre-procession...')

    if not load_data:
        if csv == cnst.FILE_TWITTER_TRAIN_DATA:
            df = process_tweets_ds(csv, csv_clean)
        elif csv == cnst.FILE_IMDB_TRAIN_DATA:
            df = process_imdb_ds(csv, csv_clean)
        else:
            df = process_tweets_ds(csv, csv_clean)
    else:
        print('Importing a cleaned dataset of tweets...')
        df = pd.read_csv(csv_clean)
        print('Our DataFrame')
        print(df.head(5))

    # The fun part starts here :)
    # Let's now visualize the data
    print('\nVisualizing data...')
    # The first graph that we'll build is "Word Cloud"
    # Some say that this type of graphs are often useless and do not provide much information,...
    # but who cares, when they look so awesome!
    # First of all, let's separate positive and negative sentences
    pos_sent = df[df.sentiment == 1]
    neg_sent = df[df.sentiment == 0]
    # Let's concatenate all the tweets in a one big string
    pos_sent = pos_sent.text.str.cat(sep=' ')
    neg_sent = neg_sent.text.str.cat(sep=' ')
    # We will now remove all the words with length <= 3
    pos_sent = re.sub(r'\b\w{1,2}\b', '', pos_sent)
    neg_sent = re.sub(r'\b\w{1,2}\b', '', neg_sent)
    # And remove unnecessary whitespaces
    pos_sent = re.sub('\s+', ' ', pos_sent).strip()
    neg_sent = re.sub('\s+', ' ', neg_sent).strip()
    # Converting strings into lists (TODO: Maybe not convert to strings at all?)
    pos_sent = pos_sent.split(' ')
    neg_sent = neg_sent.split(' ')
    # We can now start counting the words!
    pos_words = collections.Counter(pos_sent)
    neg_words = collections.Counter(neg_sent)
    # Let's get the top 100 words
    pos_words_str = ''
    neg_words_str = ''
    word_count = 200

    for t in pos_words.most_common(word_count):
        pos_words_str += t[0] + ' '

    for t in neg_words.most_common(word_count):
        neg_words_str += t[0] + ' '

    # And finally, let's make some awesome graphs
    pos_wordcloud = WordCloud(width=1200,
                              height=900,
                              max_font_size=150,
                              background_color='white').generate(pos_words_str)
    neg_wordcloud = WordCloud(width=1200,
                              height=900,
                              max_font_size=150,
                              background_color='white').generate(neg_words_str)

    fig = plt.figure(figsize=(8, 4))
    fig.add_subplot(1, 2, 1)
    plt.imshow(pos_wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.title('Positive Words')
    fig.add_subplot(1, 2, 2)
    plt.imshow(neg_wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.title('Negative Words')
    plt.show()
    print('Done with data pre-procession!')


def process_tweets_ds(csv=cnst.FILE_TWITTER_TRAIN_DATA, csv_clean=cnst.FILE_TWITTER_CLEANED_DATA):
    print('Importing a dataset of tweets...')
    df = pd.read_csv(csv, header=None, names=data_cols)  # DataFrame of Tweets

    # We will now print first 5 rows of our DataFrame
    print('Initial DataFrame: ')
    print(df.head(5))

    # Let's now print out the number of values for each sentiment
    print("\nOur dataset consists of {} tweets - {} negative, {} neutral and {} positive.\n".format(
        len(df), len(df[df.sentiment == 0]), len(df[df.sentiment == 2]), len(df[df.sentiment == 4])
    ))

    # Now we'll drop out unnecessary columns from the dataset
    df.drop(data_cols_to_drop, axis=1, inplace=True)

    # Now our DataFrame has a nicer look
    print('New look of our DataFrame after dropping out unnecessary columns:')
    print(df.head(5))

    # Let's now clean our tweets!
    print('\nCleaning up our tweets and making some other changes...')
    print('(This process can take a long time, so please be patient)\n')
    # TODO: This takes freaking much time! Should check, how I can improve it.
    cleaned_text = dataframe_cleaner(df, 'text', True)
    # Replacing old text in the DataFrame with the new list of cleaned tweets
    df.text = cleaned_text
    # Dropping out rows with empty strings
    df.dropna(inplace=True)
    df = df[df.text != '']
    # Changing positive sentiment value from 4 to 1
    df.sentiment[df.sentiment == 4] = 1
    # Saving new DataFrame to File
    df.to_csv(csv_clean)
    # Let's see what we have got!
    print('\n\nNew cleaned DataFrame')
    print(df.head(5))

    return df


def process_imdb_ds(csv=cnst.FILE_IMDB_TRAIN_DATA, csv_clean=cnst.FILE_IMDB_CLEANED_DATA):
    print('Importing a dataset of IMDB reviews...')
    df = pd.read_csv(csv)  # DataFrame of reviews

    # We will now print first 5 rows of our DataFrame
    print('Initial DataFrame: ')
    print(df.head(5))

    # Let's now print out the number of values for each sentiment
    print("\nOur dataset consists of {} reviews - {} negative, {} neutral and {} positive.\n".format(
        len(df), len(df[df.sentiment == 0]), len(df[df.sentiment == 2]), len(df[df.sentiment == 1])
    ))

    # Let's now clean our reviews!
    print('\nCleaning up our reviews and making some other changes...')
    print('(This process can take a long time, so please be patient)\n')
    # TODO: This takes freaking much time! Should check, how I can improve it.
    cleaned_text = dataframe_cleaner(df, 'text', True)
    # Replacing old text in the DataFrame with the new list of cleaned tweets
    df.text = cleaned_text
    # Dropping out rows with empty strings
    df.dropna(inplace=True)
    df = df[df.text != '']
    # Saving new DataFrame to File
    df.to_csv(csv_clean)
    # Let's see what we have got!
    print('\n\nNew cleaned DataFrame')
    print(df.head(5))

    return df


def dataframe_cleaner(dataframe, col_to_clean, print_progress):
    """ Cleans up column of a dataframe from unnecessary symbols """
    cleaned_text = []  # We will store cleaned text strings in this list
    total_string_count = len(dataframe[col_to_clean])  # Total number of strings to clean
    current_string = 0  # Current number of string which is being cleaned

    # Let's run through the DataFrame and clean text from the 'col_to_clean' column
    for text in dataframe[col_to_clean]:
        if print_progress:
            print_progress_bar(current_string, total_string_count - 1, 'Progress:', '', length=30)

        cleaned_text.append(text_cleaner(text))
        current_string += 1

    return cleaned_text


def text_cleaner(string):
    """ Cleans up text from unnecessary symbols """
    # We will start by removing HTML stuff from the text
    string = BeautifulSoup(string, 'lxml').text
    # Then we will remove tagging (@mention)
    string = re.sub(r'@[A-Za-z0-9_]+', '', string)
    # Now let's get rid of the URLs
    string = re.sub(r'https?://[^ ]+', '', string)
    string = re.sub(r'www.[^ ]+', '', string)
    # Let's lower case the text
    string = string.lower()
    # Expanding English language contractions
    for word in string.split(' '):
        if word.lower() in cnst.CONTRACTIONS:
            string = string.replace(word, cnst.CONTRACTIONS[word].lower())
    # We will now drop out hashtags and numbers
    string = re.sub('[^a-zA-Z]', ' ', string)
    # And finally get rid of unnecessary whitespaces
    string = re.sub('\s+', ' ', string).strip()

    return string


def split_the_data(x, y, split):
    rstate = RandomState(cnst.MODEL_RANDOM_STATE)  # Set random state
    indices = np.arange(x.shape[0])  # Get indicies
    rstate.shuffle(indices)  # Shuffle data
    x = x[indices]  # Shuffle tweets
    y = y[indices]  # Shuffle sentiments
    val_sample_count = int(split * x.shape[0])  # Calculate num of validation samples
    x_train = x[:-val_sample_count]  # Get train tweets
    y_train = y[:-val_sample_count]  # Get train sentiments
    x_val = x[-val_sample_count:-int(val_sample_count / 2)]  # Get validation tweets
    y_val = y[-val_sample_count:-int(val_sample_count / 2)]  # Get validation sentiments
    x_test = x[-int(val_sample_count / 2):]  # Get test tweets
    y_test = y[-int(val_sample_count / 2):]  # Get test sentiments

    return x_train, x_val, x_test, y_train, y_val, y_test


def tokenize_data(x, y,
                  num_words,
                  max_len,
                  num_classes,
                  tokenizer=None):
    """ Tokenizes the data """

    if tokenizer is None:
        tokenizer = Tokenizer(num_words=num_words)  # Use only most common words (based on frequency)
        tokenizer.fit_on_texts(x)  # Update internal vocabulary

    sequences = tokenizer.texts_to_sequences(x)  # Convert text into sequence of integers
    word_index = tokenizer.word_index  # Dict of words
    x_processed = pad_sequences(sequences, maxlen=max_len)  # Pad the sequences
    y_processed = to_categorical(np.asarray(y), num_classes)  # Convert list of sentiments to a binary matrix

    return x_processed, y_processed, word_index, tokenizer


def load_data_set(csv):
    """ Returns lists of tweets and sentiments """
    df = pd.read_csv(csv, index_col=0)  # Read data from CSV File
    x = df.text.tolist()  # Get list of tweets
    y = df.sentiment.tolist()  # Get list of sentiments

    return x, y


def get_train_val_sets(model):
    """ Returns training and validation sets """
    x, y = load_data_set(model.dataset)  # Load a dataset
    # TODO: Get a tweet with max num of words and use this num as a padding value
    # Process the data
    x_processed, y_processed, word_index, tokenizer = tokenize_data(x, y,
                                                                    model.tok_words,
                                                                    model.max_seq_length,
                                                                    model.num_classes)
    # Split the data into the train / val / test sets
    x_train, x_val, x_test, y_train, y_val, y_test = split_the_data(x_processed,
                                                                    y_processed,
                                                                    model.val_split)

    return word_index, x_train, x_val, x_test, y_train, y_val, y_test, tokenizer


def get_test_sets(model):
    """ Returns a test set """

    x, y = load_data_set(model.dataset)
    x_processed, y_processed, word_index, tokenizer = tokenize_data(x, y,
                                                                    model.tok_words,
                                                                    model.max_seq_length,
                                                                    model.num_classes,
                                                                    model.tokenizer)  # Process the data

    return x_processed, y_processed
