""" This file contains some handy functions """


def print_progress_bar(iteration, total_iterations, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """ Prints a progress bar in the console """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total_iterations)))
    filled_length = int(length * iteration // total_iterations)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r{} |{}| {}% {}'.format(prefix, bar, percent, suffix), end='\r')
