""" This file contains some constants """

import os

# region Paths
PATH_ROOT = os.path.dirname(os.path.dirname(__file__))
PATH_GUI = os.path.join(PATH_ROOT, 'gui')
PATH_ASSETS = os.path.join(PATH_GUI, 'assets')
PATH_MODELS = os.path.join(PATH_ROOT, 'models')
PATH_DATA = os.path.join(PATH_ROOT, 'data')
# endregion

# region File Locations
FILE_MAIN_GUI = os.path.join(PATH_ASSETS, 'main.ui')
FILE_LOGO = os.path.join(PATH_ASSETS, 'logo.png')
FILE_IM_EMOJI_NEG_5 = os.path.join(PATH_ASSETS, 'emoji_neg_5.png')
FILE_IM_EMOJI_POS_5 = os.path.join(PATH_ASSETS, 'emoji_pos_5.png')
FILE_IM_EMOJI_NEG_4 = os.path.join(PATH_ASSETS, 'emoji_neg_4.png')
FILE_IM_EMOJI_POS_4 = os.path.join(PATH_ASSETS, 'emoji_pos_4.png')
FILE_IM_EMOJI_NEG_3 = os.path.join(PATH_ASSETS, 'emoji_neg_3.png')
FILE_IM_EMOJI_POS_3 = os.path.join(PATH_ASSETS, 'emoji_pos_3.png')
FILE_IM_EMOJI_NEG_2 = os.path.join(PATH_ASSETS, 'emoji_neg_2.png')
FILE_IM_EMOJI_POS_2 = os.path.join(PATH_ASSETS, 'emoji_pos_2.png')
FILE_IM_EMOJI_NEG_1 = os.path.join(PATH_ASSETS, 'emoji_neg_1.png')
FILE_IM_EMOJI_POS_1 = os.path.join(PATH_ASSETS, 'emoji_pos_1.png')
FILE_IM_EMOJI_NEUTRAL = os.path.join(PATH_ASSETS, 'emoji_neutral.png')
FILE_IM_EMOJI_UNKNOWN = os.path.join(PATH_ASSETS, 'emoji_unknown.png')
# endregion

# region App
FORM_TITLE_MAIN = 'Neiron Sentiment Analyzer'
APP_ICON = os.path.join(PATH_ASSETS, 'app.ico')
# endregion

# region Emoji Texts
EMOJI_TEXTS_SAD = [
    'Pet him or something?!',
    'Just play with him already!',
    'Maybe it wants to it?',
    'Let it play some video games...',
    'I guess, it wants to go outside!',
    'Is it raining?',
    'Turn on some relaxing music.',
    'Give it a cookie',
    'It wants more positive texts!',
    'Nope! Try to be more positive!',
    'It is not feeling ok after that text...',
    'It will start to cry!',
    'Not again...',
    'It wants a taco!',
    'It wants a shawarma!',
    'It wants to go travelling.',
    'That wasn\'t the text it was waiting for!',
    'It is so alone...',
    'Look at it, it\'s so sad!'
]

EMOJI_TEXTS_VERY_SAD = [
    'Oh no, not again!',
    'What have you done?!',
    'You are a monster!',
    'You don\'t deserve it!',
    'Hurry, give it an ice-cream!!!',
    'Give it a chocolate... Tons of chocolate!',
    'Not good. Not good!',
    'Show it some funny videos!',
    'Give it some love!',
    'It wants a hug!',
    'Just leave it alone...',
    'What have you typed?!',
    'It doesn\'t want your texts anymore!',
    'Are you cheating with other emojis?!?!',
    'It wants a new IPhone!..',
    'Why are you doing this?',
    'It wants more positive texts!',
    'It is very tired...',
    'Stop doing this...',
    'Even I can\'t take this anymore...',
    'Oh crap...',
    'It wants to watch Titanic.',
    'That\'s unacceptable behaviour!',
    'Never call it again!!!',
    'Delete its phone number!'
]

EMOJI_TEXTS_HAPPY = [
    'Hurray!',
    'Nice work!',
    'Nicely done!',
    'You are a champ!',
    'It is happy ^_^',
    'It loves you!',
    'Look at its smile.',
    'So proud of you!',
    'You are the best!',
    'You know how to make your emoji smile!',
    'Yay!',
    'Awesome!'
]

EMOJI_TEXTS_VERY_HAPPY = [
    'Unbelievable!',
    'So awesome!',
    'It feels so great!',
    'You are the best emoji owner EVER!',
    'You know how to treat your emojis!',
    'WOW! Have you watched tutorials or something?',
    'Unreal! You are definitely an android!',
    'It is having so much fun!',
    'Superable!',
    'Impressive work!',
    'Wonderful!',
    'You are a badass!',
    'Excellent!',
    'Incredible!',
    'Extraordinary!!!',
    'High five!'
]

EMOJI_TEXTS_NEUTRAL = [
    'It is OK, I guess.',
    'Meh...',
    'Could be better.',
    'I don\'t even know.',
    'It\'s ok.',
    'Try harder...',
    'It has 99 problems but your text aint one.',
    'Keep it up!'
]
# enrgeion
