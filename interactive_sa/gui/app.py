""" This is the main file of the SA Tool App"""

import tkinter as tk
import pygubu
import os
import sys
sys.path.append('../')
sys.path.append('../../')
from interactive_sa import data
from interactive_sa.gui import toolbox as tbx
from interactive_sa.gui import constants as cnst
from interactive_sa import model
from random import randint
import random

class Application:
    """ Main Class """

    def __init__(self, master):
        """ Initializer """

        # Creating GUI
        # Creating a builder
        self.builder = builder = pygubu.Builder()
        # Loading an ui file
        builder.add_from_file(cnst.FILE_MAIN_GUI)
        # Creating a widget using a master as parent
        self.mainwindow = builder.get_object('frame_main', master)
        self.master = master

        # Creating Callbacks
        self.init_callbacks()

        # Displaying the logo and other images
        controls = self.builder
        self.logo = tk.PhotoImage(file=cnst.FILE_LOGO)
        self.im_emoji_neg_5 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEG_5)
        self.im_emoji_pos_5 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_POS_5)
        self.im_emoji_neg_4 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEG_4)
        self.im_emoji_pos_4 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_POS_4)
        self.im_emoji_neg_3 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEG_3)
        self.im_emoji_pos_3 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_POS_3)
        self.im_emoji_neg_2 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEG_2)
        self.im_emoji_pos_2 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_POS_2)
        self.im_emoji_neg_1 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEG_1)
        self.im_emoji_pos_1 = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_POS_1)
        self.im_emoji_neutral = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_NEUTRAL)
        self.im_emoji_unknown = tk.PhotoImage(file=cnst.FILE_IM_EMOJI_UNKNOWN)
        canvas_logo = controls.get_object('canvas_logo')
        canvas_logo.create_image(int(canvas_logo['width']) / 2, int(canvas_logo['height']) / 2,
                                 anchor=tk.CENTER, image=self.logo)
        canvas_emoji_neg = controls.get_object('canvas_emoji_neg')
        canvas_emoji_pos = controls.get_object('canvas_emoji_pos')
        self.canvas_emoji_result = controls.get_object('canvas_emoji_result')
        canvas_emoji_neg.create_image(int(canvas_emoji_neg['width']) / 2, int(canvas_emoji_neg['height']) / 2,
                                      anchor=tk.CENTER, image=self.im_emoji_neg_5)
        canvas_emoji_pos.create_image(int(canvas_emoji_pos['width']) / 2, int(canvas_emoji_pos['height']) / 2,
                                      anchor=tk.CENTER, image=self.im_emoji_pos_5)
        self.canvas_emid = self.canvas_emoji_result.create_image(int(self.canvas_emoji_result['width']) / 2,
                                                                 int(self.canvas_emoji_result['height']) / 2,
                                                                 anchor=tk.CENTER, image=self.im_emoji_unknown)

        # Getting pointers to controls
        self.combo_models = controls.get_object('combo_model')
        self.combo_dataset = controls.get_object('combo_dataset')
        self.slider_sentiment = controls.get_object('scale_sentiment')
        # Filling comboboxes
        self.init_comboboxes()

        # Our model
        self.model = None

    def init_callbacks(self):
        """ Initializes callbacks """
        # Configuring Callbacks
        callbacks = {
            # Buttons
            'on_bn_prepare_dataset': self.on_bn_prepare_dataset,
            'on_bn_load_dataset': self.on_bn_load_dataset,
            'on_bn_create_model': self.on_bn_create_model,
            'on_bn_load_model': self.on_bn_load_model,
            'on_bn_analyze': self.on_bn_analyze
        }
        self.builder.connect_callbacks(callbacks)

    def init_comboboxes(self):
        """ Adds items to corresponding comboboxes """
        models = []
        datasets = []

        for file in os.listdir(cnst.PATH_MODELS):
            if file.endswith('.h5') and not file.startswith('WEIGHTS'):
                models.append(file)

        for file in os.listdir(cnst.PATH_DATA):
            if file.endswith('.csv'):
                datasets.append(file)

        # TODO: Fill comboboxes
        self.combo_models['values'] = models
        self.combo_dataset['values'] = datasets

    def on_bn_prepare_dataset(self):
        """ Prepares dataset """
        csv_file = tbx.get_string_value(self, 'combo_dataset')
        csv_file_clean = csv_file.splt('.')[0] + '_CLEANED.csv'
        data.process(False, csv_file, csv_file_clean)

    def on_bn_load_dataset(self):
        """ Loads existing dataset """
        csv_file = os.path.join(cnst.PATH_DATA, tbx.get_string_value(self, 'combo_dataset'))
        data.process(True, csv_file, csv_file)

    def on_bn_create_model(self):
        """ Builds a model """
        self.create_model(False)

    def on_bn_load_model(self):
        """ Loads an existing model """
        self.create_model(True)

    def create_model(self, load_model):
        """ Creates a model """
        model_file = tbx.get_string_value(self, 'combo_model')
        dataset_file = os.path.join(cnst.PATH_DATA, tbx.get_string_value(self, 'combo_dataset'))
        model_id = model_file.split('.')[0]
        self.model = model.Model(model_id, dataset_file, load_model)
        self.model.create()

    def on_bn_analyze(self):
        """ Analyzes user-input text """

        if self.model is not None:
            text = self.builder.get_object('tb_text').get('1.0', tk.END)
            pred_pos = self.model.analyze_text(text)
            pred_pos = int(pred_pos * 100)
            pred_neg = 100 - pred_pos
            emoji, emoji_text = self.get_emoji_and_text(pred_pos)
            tbx.set_int_value(self, 'scale_sentiment', pred_pos)
            tbx.set_string_value(self, 'lb_result', 'Your Emoji is {}% Positive and {}% Negative.\n{}'
                               .format(pred_pos, pred_neg, emoji_text))
            self.canvas_emoji_result.delete(self.canvas_emid)
            self.canvas_emid = self.canvas_emoji_result.create_image(int(self.canvas_emoji_result['width']) / 2,
                                                                     int(self.canvas_emoji_result['height']) / 2,
                                                                     anchor=tk.CENTER, image=emoji)

    def get_emoji_and_text(self, positivity):
        if positivity in range(0, 10):
            emoji = self.im_emoji_neg_5
            text = random.choice(cnst.EMOJI_TEXTS_VERY_SAD)
        elif positivity in range(10, 20):
            emoji = self.im_emoji_neg_4
            text = random.choice(cnst.EMOJI_TEXTS_VERY_SAD)
        elif positivity in range(20, 30):
            emoji = self.im_emoji_neg_3
            text = random.choice(cnst.EMOJI_TEXTS_SAD)
        elif positivity in range(30, 40):
            emoji = self.im_emoji_neg_2
            text = random.choice(cnst.EMOJI_TEXTS_SAD)
        elif positivity in range(40, 45):
            emoji = self.im_emoji_neg_1
            text = random.choice(cnst.EMOJI_TEXTS_NEUTRAL)
        elif positivity in range(45, 55):
            emoji = self.im_emoji_neutral
            text = random.choice(cnst.EMOJI_TEXTS_NEUTRAL)
        elif positivity in range(55, 60):
            emoji = self.im_emoji_pos_1
            text = random.choice(cnst.EMOJI_TEXTS_HAPPY)
        elif positivity in range(60, 70):
            emoji = self.im_emoji_pos_2
            text = random.choice(cnst.EMOJI_TEXTS_HAPPY)
        elif positivity in range(70, 80):
            emoji = self.im_emoji_pos_3
            text = random.choice(cnst.EMOJI_TEXTS_HAPPY)
        elif positivity in range(80, 90):
            emoji = self.im_emoji_pos_4
            text = random.choice(cnst.EMOJI_TEXTS_VERY_HAPPY)
        else:
            emoji = self.im_emoji_pos_5
            text = random.choice(cnst.EMOJI_TEXTS_VERY_HAPPY)

        return emoji, text


# Main Entry Point
if __name__ == '__main__':
    root = tk.Tk()
    root.title(cnst.FORM_TITLE_MAIN)
    root.resizable(width=False, height=False)
    root.iconbitmap(default=cnst.APP_ICON)
    app = Application(root)
    root.mainloop()
