""" This file contains some handy functions """


def get_string_value(form, control):
    """ Gets string value from control """
    controls = form.builder
    return controls.get_variable(control).get()


def get_bool_value(form, control):
    """ Gets bool value from control """
    controls = form.builder
    return controls.get_variable(control).get() == 1


def get_int_value(form, control):
    """ Gets int value from control """
    controls = form.builder
    return int(controls.get_variable(control).get())


def get_float_value(form, control):
    """ Gets float value from control """
    controls = form.builder
    return float(controls.get_variable(control).get())


def is_checked(form, control):
    """ Checks if checkbox is checked """
    controls = form.builder
    return 1 if controls.get_variable(control).get() else 0


def set_string_value(form, control, value):
    """ Sets string value to control """
    set_control_value(form, control, value)


def set_control_name(form, control, name):
    """ Sets control's name """
    controls = form.builder
    controls.get_object(control)['text'] = name


def set_int_value(form, control, value):
    """ Sets int value to control """
    set_control_value(form, control, value)


def set_control_value(form, control, value):
    """ Sets control value """
    controls = form.builder
    controls.get_variable(control).set(value)


def set_checkbox_state(form, control, state):
    """ Sets checkbox state """
    controls = form.builder

    if state == 1 or state in ['select', 'selected', '1']:
        controls.get_object(control).select()
    else:
        controls.get_object(control).deselect()


def set_label_text(form, control, text):
    """ Sets label text """
    controls = form.builder
    controls.get_object(control)['text'] = text
