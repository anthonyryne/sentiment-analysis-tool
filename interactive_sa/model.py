""" This file contains model related functionality """

import os
import numpy as np
from interactive_sa import constants as cnst
from keras.models import Sequential, load_model
from keras.layers import GlobalMaxPooling1D, LSTM, Dense, Flatten, Dropout, Bidirectional, GRU
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, Embedding
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import confusion_matrix
from interactive_sa.glove import get_embeddings
from interactive_sa.data import get_train_val_sets, text_cleaner


class Model:
    def __init__(self, _id, dataset, load_model):
        self.id = _id  # Model's ID
        self.tokenizer = None  # Tokenizer
        self.model = None  # Model itself
        self.val_split = 0  # Dataset split percentage (train / validation / test)
        self.tok_words = 0  # Tokenizer words
        self.max_seq_length = 0  # Maximum length of a sequence
        self.num_classes = cnst.MODEL_SENT_COUNT  # Number of sentiment classes
        self.load_model = load_model  # Flag that indicates if model should be loaded or built from scratch
        self.name = _id + '.h5'  # Name of a model file
        self.weights_name = 'WEIGHTS_' + self.name  # Name of model's weights file
        self.dataset = dataset  # Dataset
        self.train_embedded_layer = False  # Flag that indicates if the embedded layer should be trained
        self.model_load_weights = True  # Flag that indicates if model weights should be loaded
        self.is_model_trainable = True  # Flag that shows if model is trainable

        # Initializing default values
        if self.id == cnst.MODEL_CNN_01:
            self.val_split = cnst.MODEL_CNN01_VAL_SPLIT
            self.tok_words = cnst.MODEL_CNN01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_CNN01_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_LSTM_01 or self.id == cnst.MODEL_LSTM_03:
            self.val_split = cnst.MODEL_LSTM01_VAL_SPLIT
            self.tok_words = cnst.MODEL_LSTM01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_LSTM01_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_LSTM_02:
            self.val_split = cnst.MODEL_LSTM02_VAL_SPLIT
            self.tok_words = cnst.MODEL_LSTM02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_LSTM02_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_BILSTM_01:
            self.val_split = cnst.MODEL_BILSTM01_VAL_SPLIT
            self.tok_words = cnst.MODEL_BILSTM01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_BILSTM01_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_BILSTM_02:
            self.val_split = cnst.MODEL_BILSTM02_VAL_SPLIT
            self.tok_words = cnst.MODEL_BILSTM02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_BILSTM02_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_LSTM_03:
            self.val_split = cnst.MODEL_LSTM03_VAL_SPLIT
            self.tok_words = cnst.MODEL_LSTM03_TOK_WORDS
            self.max_seq_length = cnst.MODEL_LSTM03_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_CNN_LSTM_01:
            self.val_split = cnst.MODEL_CNN_LSTM01_VAL_SPLIT
            self.tok_words = cnst.MODEL_CNN_LSTM01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_CNN_LSTM01_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_CNN_LSTM_02:
            self.val_split = cnst.MODEL_CNN_LSTM02_VAL_SPLIT
            self.tok_words = cnst.MODEL_CNN_LSTM02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_CNN_LSTM02_MAX_SEQ_LENGTH
            self.train_embedded_layer = True
        elif self.id == cnst.MODEL_GRU_01:
            self.val_split = cnst.MODEL_GRU01_VAL_SPLIT
            self.tok_words = cnst.MODEL_GRU01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_GRU01_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_GRU_02:
            self.val_split = cnst.MODEL_GRU02_VAL_SPLIT
            self.tok_words = cnst.MODEL_GRU02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_GRU02_MAX_SEQ_LENGTH
        elif self.id == cnst.MODEL_CNN_04:
            self.val_split = cnst.MODEL_CNN02_VAL_SPLIT
            self.tok_words = cnst.MODEL_CNN02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_CNN02_MAX_SEQ_LENGTH
            self.train_embedded_layer = True
        elif self.id == cnst.MODEL_PELT_LSTM_01:
            self.val_split = cnst.MODEL_PELT_LSTM01_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_LSTM01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_LSTM01_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        elif self.id == cnst.MODEL_PELT_LSTM_02:
            self.val_split = cnst.MODEL_PELT_LSTM03_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_LSTM03_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_LSTM03_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        elif self.id == cnst.MODEL_PELT_LSTM_03 \
                or self.id == cnst.MODEL_PELT_LSTM_04 \
                or self.id == cnst.MODEL_PELT_LSTM_05:
            self.val_split = cnst.MODEL_PELT_LSTM03_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_LSTM03_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_LSTM03_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        elif self.id == cnst.MODEL_PELT_CNN_01 or self.id == cnst.MODEL_PELT_CNN_02:
            self.val_split = cnst.MODEL_PELT_CNN01_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_CNN01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_CNN01_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        elif self.id == cnst.MODEL_PELT_SIMPLE_01 \
                or self.id == cnst.MODEL_PELT_SIMPLE_02 \
                or self.id == cnst.MODEL_PELT_SIMPLE_03:
            self.val_split = cnst.MODEL_PELT_SIMPLE01_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_SIMPLE01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_SIMPLE01_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        elif self.id == cnst.MODEL_PELT_MLP_01 or self.id == cnst.MODEL_PELT_MLP_02:
            self.val_split = cnst.MODEL_PELT_MLP01_VAL_SPLIT
            self.tok_words = cnst.MODEL_PELT_MLP01_TOK_WORDS
            self.max_seq_length = cnst.MODEL_PELT_MLP01_MAX_SEQ_LENGTH
            self.model_load_weights = False
            self.is_model_trainable = False
        else:
            # CNN_02, CNN_03, CNN_04
            self.val_split = cnst.MODEL_CNN02_VAL_SPLIT
            self.tok_words = cnst.MODEL_CNN02_TOK_WORDS
            self.max_seq_length = cnst.MODEL_CNN02_MAX_SEQ_LENGTH

    def create(self):
        """ Main Function """

        print('\nBuilding and evaluating a model...')
        # Let's create a dictionary of labels
        labels = {'Negative': 0, 'Positive': 1}
        # Get train and validation sets
        print('Getting and processing train, validation and test sets...')
        word_index, x_train, x_val, x_test, y_train, y_val, y_test, self.tokenizer = get_train_val_sets(self)
        evaluate = False  # Flag that indicates if model should be evaluated

        if not self.load_model:
            if self.is_model_trainable:
                # Now we can create a model...
                print('Creating a model...')
                print('****************************************')
                print('Model Parameters: ')
                print('ID: {}\nValidation Split: {}\nTokenizer Words: {}\nMax Sequence Length: {}'
                      .format(self.id,
                              self.val_split,
                              self.tok_words,
                              self.max_seq_length))
                print('****************************************')
                self.build(labels, word_index)
                # ...train it
                print('Training a model...')
                self.train(x_train, x_val, y_train, y_val)
                evaluate = True
            else:
                print('Sorry, this model is not trainable.')
                return False
        else:
            # Here we're loading an existing model
            print('Loading a model...')
            self.model = load_model(os.path.join(cnst.FOLDER_MODELS, self.name))
            if self.model_load_weights:
                self.model.load_weights(os.path.join(cnst.FOLDER_MODELS, self.weights_name))
            print('Model loaded successfully...')

        print('****************************************')
        print('Model Summary:')
        print(self.model.summary())
        print('****************************************')

        if evaluate:
            # Now we can test our model on a test set...
            print('Evaluating a model...')
            predictions = self.model.predict(x=x_test, verbose=1, batch_size=512)
            # ...and see the results
            self.evaluate(y_test, predictions)

        print('Model is ready for use!')
        return True

    def train(self, x_train, x_val, y_train, y_val):
        """ Trains a model """

        # Save the model after every epoch
        cb = [ModelCheckpoint(os.path.join(cnst.FOLDER_MODELS, self.weights_name),
                              save_best_only=True, save_weights_only=False)]
        # Fit the model
        self.model.fit(x_train, y_train, validation_data=(x_val, y_val),
                       nb_epoch=5, batch_size=256, callbacks=cb, verbose=1)

        # Remove a model file if it exists
        try:
            os.remove(os.path.join(cnst.FOLDER_MODELS, self.name))
        except OSError:
            pass

        # Save the model
        self.model.save(os.path.join(cnst.FOLDER_MODELS, self.name))

    @staticmethod
    def evaluate(real_vals, predicted_vals):
        """ Evaluates a model """

        real_sentiments = [np.argmax(x) for x in real_vals]  # Get real values
        predicted_sentiments = [np.argmax(x) for x in predicted_vals]  # Get predicted values
        res_cm = confusion_matrix(real_sentiments, predicted_sentiments)  # Create a confusion matrix
        res_good = (res_cm[0][0] + res_cm[1][1]) * 100 / res_cm.sum()  # Good results
        res_bad = (res_cm[0][1] + res_cm[1][0]) * 100 / res_cm.sum()  # Bad results
        print('\nRESULTS:\nRight Predictions - {}%'
              '\nFalse Predictions - {}%\n'.format(res_good, res_bad))  # Print results

    def build(self, labels_index, word_index):
        """ Builds a model """

        # Building an embedding layer
        embedded_sequences = self.make_embedding_layer(word_index)
        # Default values
        loss_type = 'categorical_crossentropy'

        # Building a model
        if self.id == cnst.MODEL_CNN_01:
            # Advanced CNN Model
            model = Sequential([
                embedded_sequences,
                Conv1D(filters=512, kernel_size=5, activation='relu'),
                AveragePooling1D(pool_size=5),
                Conv1D(filters=256, kernel_size=5, activation='relu'),
                AveragePooling1D(pool_size=5),
                Conv1D(filters=128, kernel_size=5, activation='relu'),
                MaxPooling1D(pool_size=5),
                Flatten(),
                Dropout(rate=0.3),
                Dense(units=128, activation='relu'),
                Dense(units=len(labels_index), activation='softmax')
            ])
        elif self.id == cnst.MODEL_LSTM_01 or self.id == cnst.MODEL_LSTM_02 \
                or self.id == cnst.MODEL_LSTM_03:
            # LSTM Models
            model = Sequential([
                embedded_sequences,
                LSTM(units=200, dropout_U=0.2, dropout_W=0.2),
                Dense(units=2, activation='softmax')
            ])
        elif self.id == cnst.MODEL_BILSTM_01:
            # BiLSTM Model
            model = Sequential([
                embedded_sequences,
                Bidirectional(LSTM(units=200, dropout_U=0.2, dropout_W=0.2)),
                Dense(units=2, activation='softmax')
            ])
        elif self.id == cnst.MODEL_BILSTM_02:
            # BiLSTM Model
            model = Sequential([
                embedded_sequences,
                Bidirectional(LSTM(units=200, dropout_U=0.2, dropout_W=0.2)),
                Dropout(0.5),
                Dense(units=2, activation='softmax')
            ])
        elif self.id == cnst.MODEL_LSTM_03:
            # Advanced LSTM Model
            model = Sequential([
                embedded_sequences,
                Dropout(0.25),
                LSTM(units=200, dropout_U=0.2, dropout_W=0.2, return_sequences=True),
                GlobalMaxPooling1D(),
                Dense(units=128, activation='relu'),
                Dense(units=2, activation='softmax')
            ])
        elif self.id == cnst.MODEL_CNN_LSTM_01 or self.id == cnst.MODEL_CNN_LSTM_02:
            # CNN + LSTM Model
            model = Sequential([
                embedded_sequences,
                Conv1D(filters=256, kernel_size=5, activation='tanh'),
                Dropout(rate=0.3),
                LSTM(units=400),
                Dense(units=128, activation='relu'),
                Dense(units=2, activation='softmax')
            ])
        elif self.id == cnst.MODEL_CNN_02 or self.id == cnst.MODEL_CNN_03 or self.id == cnst.MODEL_CNN_04:
            # Simple CNN Model
            model = Sequential([
                embedded_sequences,
                Conv1D(filters=128, kernel_size=5, activation='relu'),
                GlobalMaxPooling1D(),
                Dropout(rate=0.3),
                Dense(units=128, activation='relu'),
                Dense(units=len(labels_index), activation='softmax')
            ])
        elif self.id == cnst.MODEL_GRU_01:
            model = Sequential([
                embedded_sequences,
                GRU(50, input_shape=(300, 1), return_sequences=True),
                GRU(1, return_sequences=False),
                Dense(units=len(labels_index), activation='softmax')
            ])
        elif self.id == cnst.MODEL_GRU_02:
            model = Sequential([
                embedded_sequences,
                GRU(50, input_shape=(300, 1), return_sequences=True),
                GRU(2, return_sequences=False),
                Dense(units=len(labels_index), activation='softmax')
            ])
            loss_type = 'binary_crossentropy'
        else:
            print('No model with ID {} has been found. Using default structure.'.format(self.id))
            model = Sequential([
                embedded_sequences,
                LSTM(units=200, dropout_U=0.2, dropout_W=0.2),
                Dense(units=2, activation='softmax')
            ])

        # Compiling a model
        model.compile(loss=loss_type, optimizer='adam', metrics=['accuracy'])
        self.model = model

    def make_embedding_layer(self, word_index):
        """ Makes an embedding layer for a model """

        # Getting embeddings
        embeddings = get_embeddings()
        # Getting words
        word_count = min(self.tok_words, len(word_index))
        # Building an embedding matrix
        embedding_matrix = np.zeros((word_count, cnst.MODEL_EMBEDDING_DIM))

        for word, i in word_index.items():
            if i >= self.tok_words:
                continue

            embedding_vector = embeddings.get(word)

            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        # Creating an embedding layer
        embedding_layer = Embedding(word_count, cnst.MODEL_EMBEDDING_DIM, weights=[embedding_matrix],
                                    input_length=self.max_seq_length, trainable=self.train_embedded_layer)

        return embedding_layer

    def analyzer(self):
        """ Analyzes sentiment of a user input text """

        # Greetings
        welcome_string = 'Welcome to Interactive Sentiment Analyzer'
        print('*' * len(welcome_string))
        print(welcome_string)
        print('\nPlease write your text and hit the "Enter" button to analyze it.')
        print('Type "{}" to exit the analyzer.'.format(cnst.ANALYZER_STOPWORD))
        print('Have fun!\n')

        while True:
            # Analyzing texts
            print('-------------------------------------')
            text = input('Your text: ')  # Get text from console input

            if text == cnst.ANALYZER_STOPWORD:
                # Exits the analyzer
                break
            else:
                self.analyze_text(text)

        print('\nThanks for using Interactive Sentiment Analyzer!')

    def analyze_text(self, text):
        print('-------------------------------------')
        print('Your text is: {}'.format(text))
        text = [text_cleaner(text)]  # Clean the text and create a list
        print('Processed text: {}'.format(text[0]))
        sequence = self.tokenizer.texts_to_sequences(text)  # Convert text to sequence
        text_seq = pad_sequences(sequence, maxlen=self.max_seq_length)  # Pad the sequence
        predictions = self.model.predict(x=text_seq)  # Get a model prediction
        pred_neg = float(predictions[0][0])  # Negative portion
        pred_pos = float(predictions[0][1])  # Positive portion
        slider = round(abs(pred_neg - pred_pos) / 2 * 10 / 0.5)  # Sentiment slider

        # Printing the slider
        if pred_neg > pred_pos:
            print(':(  [' + '-' * round(10 - slider) + u'\u2588' * round(slider) + '-' * 10 + ']  :)')
        elif pred_neg < pred_pos:
            print(':(  [' + '-' * 10 + u'\u2588' * round(slider) + '-' * round(10 - slider) + ']  :)')
        else:
            print(':(  [' + '-' * 20 + ']  :)')

        print('{}% \t\t\t {}%'.format(round(pred_neg * 100, 1), round(pred_pos * 100, 1)))
        print('-------------------------------------\n')

        return pred_pos
