""" This file contains constants used in the other files """

from os import path

# region FOLDER LOCATIONS
FOLDER_ROOT = path.dirname(path.dirname(__file__))
FOLDER_APP = path.join(FOLDER_ROOT, 'interactive_sa')
FOLDER_DATA = path.join(FOLDER_APP, 'data')
FOLDER_MODELS = path.join(FOLDER_APP, 'models')
FOLDER_GLOVE = path.join(FOLDER_APP, 'glove')
# endregion

# region FILE LOCATIONS
FILE_TWITTER_TRAIN_DATA = path.join(FOLDER_DATA, 'data_utf8.csv')
FILE_TWITTER_TEST_DATA = path.join(FOLDER_DATA, 'test_data_clean.csv')
FILE_TWITTER_CLEANED_DATA = path.join(FOLDER_DATA, 'data_clean.csv')
FILE_IMDB_TRAIN_DATA = path.join(FOLDER_DATA, 'data_reviews.csv')
FILE_IMDB_CLEANED_DATA = path.join(FOLDER_DATA, 'data_reviews_clean.csv')
FILE_IMDB_TEST_DATA = path.join(FOLDER_DATA, 'data_reviews_clean.csv')
FILE_MODEL_CBOW = path.join(FOLDER_MODELS, 'w2v_model_cbow')
FILE_MODEL_SG = path.join(FOLDER_MODELS, 'w2v_model_skipgram')
FILE_GLOVE = path.join(FOLDER_GLOVE, 'glove.twitter.27B.200d.txt')
# endregion

# region MODEL IDs
MODEL_CNN_01 = 'CNN_01'
MODEL_CNN_02 = 'CNN_02'
MODEL_CNN_03 = 'CNN_03'
MODEL_CNN_04 = 'CNN_04'
MODEL_LSTM_01 = 'LSTM_01'
MODEL_LSTM_02 = 'LSTM_02'
MODEL_LSTM_03 = 'LSTM_03'
MODEL_BILSTM_01 = 'BILSTM_01'
MODEL_BILSTM_02 = 'BILSTM_02'
MODEL_CNN_LSTM_01 = 'CNN_LSTM_01'
MODEL_CNN_LSTM_02 = 'CNN_LSTM_02'
MODEL_GRU_01 = 'GRU_01'
MODEL_GRU_02 = 'GRU_02'
MODEL_PELT_LSTM_01 = 'PELT_LSTM_01'
MODEL_PELT_LSTM_02 = 'PELT_LSTM_02'
MODEL_PELT_LSTM_03 = 'PELT_LSTM_03'
MODEL_PELT_LSTM_04 = 'PELT_LSTM_04'
MODEL_PELT_LSTM_05 = 'PELT_LSTM_05'
MODEL_PELT_CNN_01 = 'PELT_CNN_01'
MODEL_PELT_CNN_02 = 'PELT_CNN_02'
MODEL_PELT_SIMPLE_01 = 'PELT_SIMPLE_01'
MODEL_PELT_SIMPLE_02 = 'PELT_SIMPLE_02'
MODEL_PELT_SIMPLE_03 = 'PELT_SIMPLE_03'
MODEL_PELT_MLP_01 = 'PELT_MLP_01'
MODEL_PELT_MLP_02 = 'PELT_MLP_02'
# endregion

# region MODEL
MODEL_CNN01_VAL_SPLIT = 0.1
MODEL_CNN01_TOK_WORDS = 20000
MODEL_CNN01_MAX_SEQ_LENGTH = 1000
MODEL_CNN02_VAL_SPLIT = 0.2
MODEL_CNN02_TOK_WORDS = 100000
MODEL_CNN02_MAX_SEQ_LENGTH = 55
MODEL_CNN03_VAL_SPLIT = MODEL_CNN02_VAL_SPLIT
MODEL_CNN03_TOK_WORDS = MODEL_CNN02_TOK_WORDS
MODEL_CNN03_MAX_SEQ_LENGTH = MODEL_CNN02_MAX_SEQ_LENGTH
MODEL_CNN04_VAL_SPLIT = MODEL_CNN02_VAL_SPLIT
MODEL_CNN04_TOK_WORDS = MODEL_CNN02_TOK_WORDS
MODEL_CNN04_MAX_SEQ_LENGTH = MODEL_CNN02_MAX_SEQ_LENGTH
MODEL_LSTM01_VAL_SPLIT = 0.2
MODEL_LSTM01_TOK_WORDS = 100000
MODEL_LSTM01_MAX_SEQ_LENGTH = 55
MODEL_LSTM02_VAL_SPLIT = 0.2
MODEL_LSTM02_TOK_WORDS = 30000
MODEL_LSTM02_MAX_SEQ_LENGTH = 55
MODEL_LSTM03_VAL_SPLIT = 0.2
MODEL_LSTM03_TOK_WORDS = 100000
MODEL_LSTM03_MAX_SEQ_LENGTH = 55
MODEL_BILSTM01_VAL_SPLIT = 0.2
MODEL_BILSTM01_TOK_WORDS = 100000
MODEL_BILSTM01_MAX_SEQ_LENGTH = 55
MODEL_BILSTM02_VAL_SPLIT = 0.2
MODEL_BILSTM02_TOK_WORDS = 100000
MODEL_BILSTM02_MAX_SEQ_LENGTH = 60
MODEL_CNN_LSTM01_VAL_SPLIT = 0.2
MODEL_CNN_LSTM01_TOK_WORDS = 100000
MODEL_CNN_LSTM01_MAX_SEQ_LENGTH = 55
MODEL_CNN_LSTM02_VAL_SPLIT = 0.2
MODEL_CNN_LSTM02_TOK_WORDS = 100000
MODEL_CNN_LSTM02_MAX_SEQ_LENGTH = 55
MODEL_GRU01_VAL_SPLIT = 0.2
MODEL_GRU01_TOK_WORDS = 80000
MODEL_GRU01_MAX_SEQ_LENGTH = 60
MODEL_GRU02_VAL_SPLIT = 0.2
MODEL_GRU02_TOK_WORDS = 80000
MODEL_GRU02_MAX_SEQ_LENGTH = 60
MODEL_PELT_LSTM01_VAL_SPLIT = 0.2
MODEL_PELT_LSTM01_TOK_WORDS = 100000
MODEL_PELT_LSTM01_MAX_SEQ_LENGTH = 55
MODEL_PELT_LSTM02_VAL_SPLIT = 0.2
MODEL_PELT_LSTM02_TOK_WORDS = 100000
MODEL_PELT_LSTM02_MAX_SEQ_LENGTH = 55
MODEL_PELT_LSTM03_VAL_SPLIT = 0.2
MODEL_PELT_LSTM03_TOK_WORDS = 100000
MODEL_PELT_LSTM03_MAX_SEQ_LENGTH = 55
MODEL_PELT_CNN01_VAL_SPLIT = 0.2
MODEL_PELT_CNN01_TOK_WORDS = 100000
MODEL_PELT_CNN01_MAX_SEQ_LENGTH = 55
MODEL_PELT_MLP01_VAL_SPLIT = 0.2
MODEL_PELT_MLP01_TOK_WORDS = 100000
MODEL_PELT_MLP01_MAX_SEQ_LENGTH = 55
MODEL_PELT_SIMPLE01_VAL_SPLIT = 0.2
MODEL_PELT_SIMPLE01_TOK_WORDS = 100000
MODEL_PELT_SIMPLE01_MAX_SEQ_LENGTH = 55
MODEL_SENT_COUNT = 2
MODEL_RANDOM_STATE = 9999
MODEL_EMBEDDING_DIM = 200
# endregion

# region Analyzer
ANALYZER_STOPWORD = 'EXIT'
# endregion

# region REGEX
CONTRACTIONS = {
    "ain't": "not",
    "aren't": "are not",
    "can't": "cannot",
    "can't've": "cannot have",
    "'cause": "because",
    "could've": "could have",
    "couldn't": "could not",
    "couldn't've": "could not have",
    "didn't": "did not",
    "doesn't": "does not",
    "don't": "do not",
    "hadn't": "had not",
    "hadn't've": "had not have",
    "hasn't": "has not",
    "haven't": "have not",
    "he'd": "he had",
    "he'd've": "he would have",
    "he'll": "he will",
    "he'll've": "he will have",
    "he's": "he is",
    "how'd": "how did",
    "how'd'y": "how do you",
    "how'll": "how will",
    "how's": "how is",
    "i'd": "i had",
    "i'd've": "i would have",
    "i'll": "i will",
    "i'll've": "i will have",
    "i'm": "i am",
    "i've": "i have",
    "isn't": "is not",
    "it'd": "it would",
    "it'd've": "it would have",
    "it'll": "it will",
    "it'll've": "it will have",
    "it's": "it is",
    "let's": "let us",
    "ma'am": "madam",
    "mayn't": "may not",
    "might've": "might have",
    "mightn't": "might not",
    "mightn't've": "might not have",
    "must've": "must have",
    "mustn't": "must not",
    "mustn't've": "must not have",
    "needn't": "need not",
    "needn't've": "need not have",
    "o'clock": "of the clock",
    "oughtn't": "ought not",
    "oughtn't've": "ought not have",
    "shan't": "shall not",
    "sha'n't": "shall not",
    "shan't've": "shall not have",
    "she'd": "she would",
    "she'd've": "she would have",
    "she'll": "she will",
    "she'll've": "she will have",
    "she's": "she is",
    "should've": "should have",
    "shouldn't": "should not",
    "shouldn't've": "should not have",
    "so've": "so have",
    "so's": "so is",
    "that'd": "that would",
    "that'd've": "that would have",
    "that's": "that is",
    "there'd": "there would",
    "there'd've": "there would have",
    "there's": "there is",
    "they'd": "they would",
    "they'd've": "they would have",
    "they'll": "they will",
    "they'll've": "they will have",
    "they're": "they are",
    "they've": "they have",
    "to've": "to have",
    "wasn't": "was not",
    "we'd": "we would",
    "we'd've": "we would have",
    "we'll": "we will",
    "we'll've": "we will have",
    "we're": "we are",
    "we've": "we have",
    "weren't": "were not",
    "what'll": "what will",
    "what'll've": "what will have",
    "what're": "what are",
    "what's": "what is",
    "what've": "what have",
    "when's": "when is",
    "when've": "when have",
    "where'd": "where did",
    "where's": "where is",
    "where've": "where have",
    "who'll": "who will",
    "who'll've": "who will have",
    "who's": "who is",
    "who've": "who have",
    "why's": "why is",
    "why've": "why have",
    "will've": "will have",
    "won't": "will not",
    "won't've": "will not have",
    "would've": "would have",
    "wouldn't": "would not",
    "wouldn't've": "would not have",
    "y'all": "you all",
    "y'all'd": "you all would",
    "y'all'd've": "you all would have",
    "y'all're": "you all are",
    "y'all've": "you all have",
    "you'd": "you would",
    "you'd've": "you would have",
    "you'll": "you will",
    "you'll've": "you will have",
    "you're": "you are",
    "you've": "you have",
    "aint": "not",
    "arent": "are not",
    "cant": "cannot",
    "cantve": "cannot have",
    "cause": "because",
    "couldve": "could have",
    "couldnt": "could not",
    "couldntve": "could not have",
    "didnt": "did not",
    "doesnt": "does not",
    "dont": "do not",
    "hadnt": "had not",
    "hadntve": "had not have",
    "hasnt": "has not",
    "havent": "have not",
    "hellve": "he will have",
    "howd": "how did",
    "howdy": "how do you",
    "howll": "how will",
    "hows": "how is",
    "idve": "i would have",
    "illve": "i will have",
    "im": "i am",
    "ive": "i have",
    "isnt": "is not",
    "itdve": "it would have",
    "itll": "it will",
    "itllve": "it will have",
    "lets": "let us",
    "maam": "madam",
    "maynt": "may not",
    "mightve": "might have",
    "mightnt": "might not",
    "mightntve": "might not have",
    "mustve": "must have",
    "mustnt": "must not",
    "mustntve": "must not have",
    "neednt": "need not",
    "needntve": "need not have",
    "oclock": "of the clock",
    "oughtnt": "ought not",
    "oughtntve": "ought not have",
    "shant": "shall not",
    "shantve": "shall not have",
    "shedve": "she would have",
    "shellve": "she will have",
    "shes": "she is",
    "shouldve": "should have",
    "shouldnt": "should not",
    "shouldntve": "should not have",
    "thatd": "that would",
    "thatdve": "that would have",
    "thats": "that is",
    "thered": "there would",
    "theredve": "there would have",
    "theres": "there is",
    "theyd": "they would",
    "theydve": "they would have",
    "theyll": "they will",
    "theyllve": "they will have",
    "theyre": "they are",
    "theyve": "they have",
    "wasnt": "was not",
    "wedve": "we would have",
    "wellve": "we will have",
    "weve": "we have",
    "werent": "were not",
    "whatll": "what will",
    "whatllve": "what will have",
    "whatre": "what are",
    "whats": "what is",
    "whatve": "what have",
    "whens": "when is",
    "whenve": "when have",
    "whered": "where did",
    "wheres": "where is",
    "whereve": "where have",
    "wholl": "who will",
    "whollve": "who will have",
    "whove": "who have",
    "whys": "why is",
    "whyve": "why have",
    "willve": "will have",
    "wont": "will not",
    "wontve": "will not have",
    "wouldve": "would have",
    "wouldnt": "would not",
    "wouldntve": "would not have",
    "yall": "you all",
    "yalld": "you all would",
    "yalldve": "you all would have",
    "yallre": "you all are",
    "yallve": "you all have",
    "youd": "you would",
    "youdve": "you would have",
    "youll": "you will",
    "youllve": "you will have",
    "youre": "you are",
    "youve": "you have"
}
# endregion
